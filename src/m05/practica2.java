package m05;

public class practica2 {
    public class P2 {
        public int metodeRaro(int opcio, String paraula, char lletra){
            if (opcio == 0)
                return paraula.length();
            else {
                int coincidencies=0;
                for (int i = 0; i < paraula.length(); i++) {
                    if (paraula.charAt(i) == lletra) coincidencies++;
                }
                if (coincidencies > 5) return -1;
                return coincidencies;
            }
        }
    }
}
