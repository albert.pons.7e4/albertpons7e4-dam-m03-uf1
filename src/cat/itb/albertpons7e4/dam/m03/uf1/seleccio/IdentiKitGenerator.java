package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IdentiKitGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String cabells = scanner.next();
        String ulls = scanner.next();
        String nas = scanner.next();
        String boca = scanner.next();

        switch (cabells) {
            case "arrissats":
                System.out.println("@@@@@");
                break;
            case "llisos":
                System.out.println("VVVVV");
                break;
            case "pentinats":
                System.out.println("XXXXX");
                break;
        }
        switch (ulls) {
            case "aclucats":
                System.out.println(".-.-.");
                break;
            case "rodons":
                System.out.println(".o-o.");
                break;
            case "estrellats":
                System.out.println(".*-*.");
                break;
        }
        switch (nas) {
            case "aixafat":
                System.out.println("..0..");
                break;
            case "arromangat":
                System.out.println("..C..");
                break;
            case "aguilenc":
                System.out.println("..V..");
                break;
        }
        switch (boca){
            case "normal" :
                System.out.println(".===.");
                break;
            case "bigoti" :
                System.out.println(".∼∼∼.");
                break;
            case "dents-sortides" :
                System.out.println(".WWW.");
        }
    }
}
