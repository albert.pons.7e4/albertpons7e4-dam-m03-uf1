package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class ExamGrade {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        float nota = scanner.nextFloat();


        if (nota >=9)
            System.out.println("execelent");
        else if (nota >=7)
            System.out.println("notable");
        else if (nota >= 6)
            System.out.println("bé");
        else if (nota >= 5)
            System.out.println("suficient");
        else if (nota >=0)
            System.out.println("suspès");
        else System.out.println("nota no vàlida");
    }
}
