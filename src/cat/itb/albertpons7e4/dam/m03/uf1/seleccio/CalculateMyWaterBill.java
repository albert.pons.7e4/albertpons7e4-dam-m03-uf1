package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class CalculateMyWaterBill {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String tipus = scanner.next();
        double m3 = scanner.nextDouble();

        System.out.println("habitatge tipus: " + tipus + " amb un gast de: " + m3 + "m3 d'aigua");

        if(tipus.equals("a")){
            m3 = m3*0.25;
        }
        else if(tipus.equals("b")){
            m3 = m3*0.33;
        }
        else if(tipus.equals("c")){
            m3 = m3*0.4;
        }
        else if(tipus.equals("d")){
            m3 = m3*0.5;
        }
        else if(tipus.equals("e")){
            m3 = m3*0.63;
        }
        else if(tipus.equals("f")){
            m3 = m3*1;
        }
        else if(tipus.equals("g")){
            m3 = m3*1.6;
        }
        else if(tipus.equals("h")){
            m3 = m3*2.5;
        }
        else if(tipus.equals("i")){
            m3 = m3*4;
        }

        System.out.println("has de pagar:  "+m3+"€");
    }
}
