package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();

        int absoluteValue = 0;
        if(num > 0)
            absoluteValue = num;
        else
            absoluteValue = -num;

        System.out.println(absoluteValue);
    }
}
