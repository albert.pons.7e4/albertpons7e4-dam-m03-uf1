package cat.itb.albertpons7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;

public class RadarDetector {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double speed = scanner.nextDouble();

        if(speed >140){
            System.out.println("Multa greu");
        }
        else if(speed > 120){
            System.out.println("Multa lleu");
        }
        else if(speed<=120){
            System.out.println("Correcte");
        }
    }
}
