package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class MinimumNotesAndCoins {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double diners = scanner.nextDouble();
        double resto = 0;

        if (diners >= 500){

            int bill500 = (int) (diners/500);
            resto = diners % 500;


            System.out.println(bill500 + " billets de 500");

        }
        if(resto >= 200){

            int bill200 = (int) (resto/200);
            resto = resto % 200;

            System.out.println(bill200 + " billets de 200");
        }
        if(resto >= 100){

            int bill100 = (int) (resto/100);
            resto = resto % 100;

            System.out.println(bill100 + " billets de 100");
        }
        if(resto >= 50){

            int bill50 = (int) (resto/50);
            resto = resto % 50;

            System.out.println(bill50 + " billets de 50");
        }
        if(resto >= 20){

            int bill20 = (int) (resto/20);
            resto = resto % 20;

            System.out.println(bill20 + " billets de 20");
        }
        if(resto >= 10){

            int bill10 = (int) (resto/10);
            resto = resto % 10;

            System.out.println(bill10 + " billets de 10");
        }
        if(resto >= 5){

            int bill5 = (int) (resto/5);
            resto = resto % 5;

            System.out.println(bill5 + " billets de 5");
        }
        if(resto >= 2){

            int coin2 = (int) (resto/2);
            resto = resto % 2;

            System.out.println(coin2 + " monedes de 2");
        }
        if(resto >= 1){

            int coin1 = (int) (resto/1);
            resto = resto % 1;

            System.out.println(coin1 + " monedes de 1");
        }
        if(resto >= 0.5){

            int ctm50 = (int) (resto/0.5);
            resto = resto % 0.5;

            System.out.println(ctm50 + " centims de 50");
        }
        if(resto >= 0.2){

            int ctm20 = (int) (resto/0.2);
            resto = resto % 0.2;

            System.out.println(ctm20 + " centims de 20");
        }
        if(resto >= 0.1){

            int ctm10 = (int) (resto/0.1);
            resto = resto % 0.1;

            System.out.println(ctm10 + " centims de 10");
        }
        if(resto >= 0.05){

            int ctm5 = (int) (resto/0.05);
            resto = resto % 0.05;

            System.out.println(ctm5 + " centims de 5");
        }
        if(resto >= 0.02){

            int ctm2 = (int) (resto/0.02);
            resto = resto % 0.02;

            System.out.println(ctm2 + " centims de 2");
        }
        if(resto >= 0.01){

            int ctm1 = (int) (resto/0.01);

            System.out.println(ctm1 + " centims de 1");
        }
    }
}
