package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IsLeapYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int any = scanner.nextInt();

        boolean isLeap = (any % 4 == 0) && ((any % 100 != 0) || (any % 400 == 0));

        if(isLeap)
            System.out.println(any + " és any de trespàs");

        else
            System.out.println(any + " no és any de trespàs");




        // és divisible entre 4, no es divisible entre 100, es divisible entre 400
    }
}

