package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int primer = scanner.nextInt();
        int segon = scanner.nextInt();

        if(primer == segon)
            System.out.println("empat");
        else if( (primer == 2 && segon == 1)
            || (primer == 1 && segon == 3)
            || (primer == 3 && segon == 2))
            System.out.println("guanya el primer");
        else
            System.out.println("guanya el segon");

    }
}
