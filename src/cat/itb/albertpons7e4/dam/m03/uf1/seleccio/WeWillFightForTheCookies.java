package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WeWillFightForTheCookies {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int Persones = scanner.nextInt();
        int Galetes = scanner.nextInt();

        int nombreGaletesPersona = Galetes / Persones;
        int galetesSobrants = Galetes % Persones;

        System.out.printf("Galetes per persona: %d\n", nombreGaletesPersona);
        System.out.printf("Galetes sobrants: %d\n", galetesSobrants);

        if(galetesSobrants == 0)
            System.out.println("A menjar");
        else
            System.out.println("A lluitar");
    }
}
