package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NextSecond {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int hora = scanner.nextInt();
        int minut = scanner.nextInt();
        int segon = scanner.nextInt();

        System.out.printf("son les %02d:%02d:%02d\n", hora, minut, segon);

        segon ++;
        if(segon >= 60){
            segon = 0;
            minut ++;
        }
        if(minut >= 60){
            minut = 0;
            hora ++;
        }
        if(hora >= 24){
            hora = 0;
        }

        System.out.printf("son les %02d:%02d:%02d\n", hora, minut, segon);
    }
}
