package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class WhichPizzaShouldIBuy {
    public static void main(String[] args) {

        Scanner num = new Scanner(System.in);

        float dro = num.nextFloat();
        float dreA = num.nextFloat();
        float dreB = num.nextFloat();

        float aro = (float) (pow(dro/2, 2)*(PI));
        float are = (float) (dreA*dreB);

        if (aro > are) System.out.println("comprar la rodona");
        else System.out.println("comprar la rectangular");
    }
}
