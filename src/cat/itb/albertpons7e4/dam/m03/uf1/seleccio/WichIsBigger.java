package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WichIsBigger {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();

        int res;

        if (n1 > n2)
            res = n1;
        else
            res = n2;

        System.out.println(res);
    }
}
