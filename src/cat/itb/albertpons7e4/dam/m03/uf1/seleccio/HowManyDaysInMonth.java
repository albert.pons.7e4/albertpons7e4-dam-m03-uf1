package cat.itb.albertpons7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class HowManyDaysInMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int mes = scanner.nextInt();

        switch(mes){
            case 1 :
            case 3 :
            case 5 :
            case 7 :
            case 8 :
            case 10 :
            case 12 :
                System.out.println("Aquest mes te 31 dies");
                break;
            case 2 :
                System.out.println("Aquest mes te 29 dies");
                break;
            case 4 :
            case 6 :
            case 9 :
            case 11 :
                System.out.println("Aquest mes te 30 dies");
        }
    }
}
