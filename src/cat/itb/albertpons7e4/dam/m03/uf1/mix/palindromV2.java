package cat.itb.albertpons7e4.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class palindromV2 {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        String input = lector.nextLine();
        List<Character> list = new ArrayList<Character>();
        boolean palindrom = false;

        //Creo un dynamic array de 'char' en base a la 'String' introducida.
        for (int i = 0; i < input.length(); i++) {
            list.add(input.charAt(i));
        }

        //Elimino cualquier carácter no válido a tener en cuenta.
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i)) {
                case '.':
                case ',':
                case '\'':
                case '!':
                case '?':
                case '-':
                case ' ':
                    list.remove(i);
            }
        }

        //Check de si la primera posición coincide con la ultima, la segunda con la penúltima... Así hasta 'size() / 2'.
        for (int i = 0; i < list.size() / 2; i++) {
            palindrom = list.get(i).equals(list.get(list.size() - 1 - i));
            if (!palindrom)
                break;
        }
        System.out.println(palindrom);
        /*
         * Algunos ejemplos de input:
         * hola mon!
         * luz azul
         * amor-a 'roma
         *
         * Output de los inputs:
         * false
         * true
         * true
         */
    }
}
