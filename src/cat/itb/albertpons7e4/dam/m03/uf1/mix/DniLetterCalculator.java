package cat.itb.albertpons7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class DniLetterCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] taula = {"T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E"};

        int num = scanner.nextInt();
        int dni = num%23;

        String lletra = taula[dni];

        System.out.println(num+lletra);


    }
}
