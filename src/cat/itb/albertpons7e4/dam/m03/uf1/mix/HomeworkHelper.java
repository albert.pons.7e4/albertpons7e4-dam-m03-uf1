package cat.itb.albertpons7e4.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HomeworkHelper {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> list = new ArrayList<Integer>();

        int D = scanner.nextInt();
        int d;
        int q;
        int r;
        int num = 0;

        System.out.println("");

        while (D != -1) {
            list.add(D);
            d = scanner.nextInt();
            list.add(d);
            q = scanner.nextInt();
            list.add(q);
            r = scanner.nextInt();
            list.add(r);

            if (list.get(num) / list.get(num + 1) == list.get(num + 2) && list.get(num) % list.get(num + 1) == list.get(num + 3)) {
                System.out.println("correcte");
            } else {
                System.out.println("error");
            }
            num += 4;

            D = scanner.nextInt();
        }
    }
}