package cat.itb.albertpons7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class BasketballDecriptor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int un = 0;
        int dos = 0;
        int tres = 0;
        int total = 0;
        int punts = scanner.nextInt();

        while(punts != -1){
            punts = punts-total;
            if(punts == 1)
                un++;
            if (punts == 2)
                dos++;
            if (punts == 3)
                tres++;
            total = total+punts;
            punts = scanner.nextInt();
        }
        System.out.println("cistelles d'un punt: "+un);
        System.out.println("cistelles de dos punts: "+dos);
        System.out.println("cistelles de tres punts: "+tres);
    }
}
