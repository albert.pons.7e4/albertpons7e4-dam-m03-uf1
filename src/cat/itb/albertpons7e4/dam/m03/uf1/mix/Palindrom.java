package cat.itb.albertpons7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String frase = scanner.nextLine().replaceAll("[^a-zA-Z]","");
        String reverse = new StringBuffer(frase).reverse().toString();

        if(frase.equals(reverse))
            System.out.println("true");
        else
            System.out.println("false");

    }
}
