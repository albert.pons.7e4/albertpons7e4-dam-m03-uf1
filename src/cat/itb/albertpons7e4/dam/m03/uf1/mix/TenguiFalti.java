package cat.itb.albertpons7e4.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TenguiFalti {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> cromos1 = new ArrayList<>();
        List<Integer> cromos2 = new ArrayList<>();

        int num = scanner.nextInt();

        while(num != -1){
            cromos1.add(num);
            num = scanner.nextInt();
        }
        int num2 = scanner.nextInt();
        while(num2 != -1){
            cromos2.add(num2);
            num2 = scanner.nextInt();
        }

        cromos1.retainAll(cromos2);
        System.out.println(cromos1);
    }
}
