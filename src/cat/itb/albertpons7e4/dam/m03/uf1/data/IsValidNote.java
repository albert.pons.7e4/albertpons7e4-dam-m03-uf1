package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsValidNote {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int valorBillet = scanner.nextInt();

        boolean isValidNote = valorBillet == 5
                || valorBillet == 10
                || valorBillet == 20
                || valorBillet == 50
                || valorBillet == 100
                || valorBillet == 200
                || valorBillet == 500;

        System.out.println(valorBillet);
    }
}
