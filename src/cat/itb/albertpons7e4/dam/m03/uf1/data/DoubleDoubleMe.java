package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class DoubleDoubleMe {
    public static void main(String[] args) {
        System.out.println("et duplicare el valor decimal que em donis");

        Scanner num = new Scanner(System.in);
        float a = num.nextFloat();

        System.out.println(a*2);
    }
}
