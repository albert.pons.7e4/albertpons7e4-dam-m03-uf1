package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class HelloToMe {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String nom = scanner.next();

        System.out.println("Bon dia " + nom);
    }
}
