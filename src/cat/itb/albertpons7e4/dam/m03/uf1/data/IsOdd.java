package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsOdd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();

        if (num % 2 == 1){

            System.out.println(true);

        }else{
            System.out.println(false);

        }

    }
}
