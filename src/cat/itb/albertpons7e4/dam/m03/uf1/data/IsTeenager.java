package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsTeenager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int edat = scanner.nextInt();

        boolean majorQue = edat > 10;
        boolean menorQue = edat < 20;

        System.out.println(majorQue && menorQue);

    }
}
