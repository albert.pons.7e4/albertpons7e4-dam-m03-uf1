package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class NextNumber {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();

        System.out.println("El següent numero és: "+(num+1));
    }
}
