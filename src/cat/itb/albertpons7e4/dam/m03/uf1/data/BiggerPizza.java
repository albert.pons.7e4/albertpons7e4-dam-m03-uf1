package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class BiggerPizza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double diametre = scanner.nextDouble();
        double supRodona = Math.PI * Math.pow(diametre/2, 2);

        double base = scanner.nextDouble();
        double altura = scanner.nextDouble();
        double supQuadrada = base * altura;

        boolean biggerPizza = supRodona > supQuadrada;
        System.out.println(biggerPizza);
    }

}
