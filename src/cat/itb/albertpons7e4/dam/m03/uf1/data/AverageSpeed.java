package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class AverageSpeed {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double km = scanner.nextDouble();
        double min = scanner.nextDouble();

        double hores = min/60;

        System.out.println(km/hores + "km/h");
    }
}
