package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsGreater {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        boolean isGreater = num1 > num2;
        System.out.println(isGreater);
    }
}
