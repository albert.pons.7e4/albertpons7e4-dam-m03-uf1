package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntRectangleArea {
    public static void main(String[] args) {
        System.out.println("escriu la base i l'altura d'un rectangle i et donare l'area");

        Scanner num1 = new Scanner(System.in);

        int b = num1.nextInt();

        Scanner num2 = new Scanner(System.in);

        int a = num2.nextInt();

        System.out.println("l'area del rectangle es:");
        System.out.println(b*a);
    }
}
