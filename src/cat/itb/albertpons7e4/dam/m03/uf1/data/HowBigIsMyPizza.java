package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

import static java.lang.Math.PI;

public class HowBigIsMyPizza {

    public static void main(String[] args) {
        System.out.println("donem el diametre de la pizza i et calculo la superficie");

        Scanner num = new Scanner(System.in);
        float a = num.nextFloat();

        System.out.println(PI*((a*a)/4));
        System.out.print("m2");
    }
}
