package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntDoubleMe {
    public static void main(String[] args) {
        System.out.println("escriu un numero i te'l duplico");
        Scanner numero = new Scanner(System.in);

        int cosa = numero.nextInt();

        System.out.println(cosa*2);
    }
}