package cat.itb.albertpons7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class OneIs10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        int num3 = scanner.nextInt();
        int num4 = scanner.nextInt();


        boolean OneIs10 = num1 == 10
                || num2 == 10
                || num3 == 10
                || num4 == 10;

        System.out.println(OneIs10);
    }
}
