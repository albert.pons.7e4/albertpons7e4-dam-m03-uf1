package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        do{
            System.out.println("Introdueix un múmero entre 1 i 5");
        }while(1<=n && n<=5);
    }
}
