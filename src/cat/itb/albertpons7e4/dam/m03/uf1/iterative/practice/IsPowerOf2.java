package cat.itb.albertpons7e4.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class IsPowerOf2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        while(num%2 == 0){
            num /= 2;

        }
        if(num == 1 & num == 0)
            System.out.println("true");
        else
            System.out.println("false");
    }
}
