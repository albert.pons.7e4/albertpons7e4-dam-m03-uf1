package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class CountWithJumps {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int salt = scanner.nextInt();

        for(int i=1; i<=n; i=i+salt){
            System.out.print(i+" ");
        }

    }
}
