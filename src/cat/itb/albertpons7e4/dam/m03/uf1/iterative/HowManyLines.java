package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = 0;
        String linea = scanner.nextLine();

        while(!linea.equals("END")){
            linea = scanner.nextLine();
            n++;
        }

        System.out.println(n);
    }
}
