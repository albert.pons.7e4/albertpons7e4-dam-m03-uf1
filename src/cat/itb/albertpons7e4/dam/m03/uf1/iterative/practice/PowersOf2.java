package cat.itb.albertpons7e4.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class PowersOf2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int exp = scanner.nextInt();

        int elevat = 1;
        int pow = 1;
        System.out.println("2^0 = 1");
        for(int i = 1; i < exp; i++) {
            pow = pow*2;
            if(!(exp == 1))
            System.out.println("2^" + elevat + " = " + pow);
            elevat++;
        }
    }
}
