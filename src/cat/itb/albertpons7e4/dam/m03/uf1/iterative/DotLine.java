package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class DotLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int dots = scanner.nextInt();


        for(int i=0; i<dots; i++){
            System.out.print(".");
        }
    }
}
