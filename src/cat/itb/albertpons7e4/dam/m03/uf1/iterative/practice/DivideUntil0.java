package cat.itb.albertpons7e4.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class DivideUntil0 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        int div = 0;
        int rest = 0;

        while(num != 0){
            if(num % 2 == 0){
                div++;
                num = num/2;
            }else{
                rest++;
                num = num-1;
            }
        }

        System.out.println("Divisions: "+div);
        System.out.println("Restes: "+rest);
    }
}
