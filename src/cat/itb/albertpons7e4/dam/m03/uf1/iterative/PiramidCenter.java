package cat.itb.albertpons7e4.dam.m03.uf1.iterative;


import java.util.Scanner;

public class PiramidCenter {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        int rows = lector.nextInt(), space;

        //Printa el número de filas introducido
        for (int i = 0; i < rows; i++) {
            //Printa espacios hasta 'rows' - i ej: si i = 2 y rows = 5 -> hará 5 - 2 = 3 espacios
            for (space = 0; space <= rows - i; space++) {
                System.out.print(" ");
            }
            //Printa almoadillas hasta 'rows' -> en la primera linea printa rows - rows - i = 1 almoadillas
            for (int j = rows; j >= rows - i; j--) {
                System.out.print("# ");
            }
            System.out.println();
        }
    }
}
