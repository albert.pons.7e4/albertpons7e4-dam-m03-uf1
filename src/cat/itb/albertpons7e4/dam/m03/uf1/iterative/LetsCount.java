package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int i = 1;

        while(i<=n){
            System.out.print(i);
            i++;
        }

        System.out.println();
    }
}
