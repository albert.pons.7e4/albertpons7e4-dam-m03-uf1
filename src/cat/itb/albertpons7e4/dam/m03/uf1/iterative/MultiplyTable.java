package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();
        int i = 1;
        int a = 1;

        while(i<10){
            a = num*i;
            i++;
            System.out.println(i + " * " + num + " = " + a);
        }

    }
}
