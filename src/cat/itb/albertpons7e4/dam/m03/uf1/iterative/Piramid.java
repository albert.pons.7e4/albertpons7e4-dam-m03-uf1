package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class Piramid {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in);
        int nivell = scanner.nextInt();

        for(int i = 0; i<=nivell; i++){
            for(int j = 1; j <= i; ++j){
                System.out.print("# ");
            }
            System.out.println();
        }
    }
}
