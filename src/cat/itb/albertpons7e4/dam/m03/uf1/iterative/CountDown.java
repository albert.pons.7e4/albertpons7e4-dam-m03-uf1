package cat.itb.albertpons7e4.dam.m03.uf1.iterative;

import java.util.Scanner;

public class CountDown {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for(int i=n-1; i>0; i--)
            System.out.print(i);
    }
}
