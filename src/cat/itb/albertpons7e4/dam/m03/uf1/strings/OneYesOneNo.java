package cat.itb.albertpons7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class OneYesOneNo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String paraula = scanner.next();


        for (int i = 0; i <= paraula.length(); i++) {
            if(i % 2 == 0) {
                char cha = paraula.charAt(i);
                System.out.print(cha);
            }
        }
        System.out.println();
    }
}
