package cat.itb.albertpons7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HiddenNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numUsu = scanner.nextInt();

        int numPc = (int) Math.random()*3+1;

        if(numUsu == numPc){
            System.out.println("L'has encertat");
        }
        else{
            System.out.println("No l'has encertat");
        }
    }
}
