package cat.itb.albertpons7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class PowerOf {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(Math.pow(a,b));
    }
}
