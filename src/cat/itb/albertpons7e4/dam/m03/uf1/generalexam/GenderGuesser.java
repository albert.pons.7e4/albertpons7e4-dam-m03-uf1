package cat.itb.albertpons7e4.dam.m03.uf1.generalexam;

import java.util.Scanner;

public class GenderGuesser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String word = scanner.next();

        while(!word.equals("END")){
            if (word.endsWith("a"))
                System.out.println("femení");
            else if (word.endsWith("s"))
                System.out.println("plural");
            else
                System.out.println("masculí");
            word = scanner.next();
        }
    }
}
