package cat.itb.albertpons7e4.dam.m03.uf1.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HeartRateWarning {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int min = scanner.nextInt();
        int max = scanner.nextInt();

        int puls = scanner.nextInt();

        int timesMin = 0;
        int timesMax = 0;

        List<Integer> list = new ArrayList<Integer>();

        while(puls != -1){
            if (puls < min){
                timesMin++;
            }
            if (puls > max){
                timesMax++;
            }
            if (list.size()-1 <min) {
                if (timesMin == 3) {
                    System.out.println("MASSA BAIX");
                    timesMin = 0;
                }
            }
            if (list.size()-1 <max) {
                if (timesMax == 3) {
                    System.out.println("MASSA ALT");
                    timesMax = 0;
                }
            }
            list.add(puls);
            puls = scanner.nextInt();
        }
    }
}
