package cat.itb.albertpons7e4.dam.m03.uf1.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BasketballTournamentManager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] x = new int[8];

        int y = scanner.nextInt();


        while(y != -1){
            x[y-1]++;
            System.out.println("L'equip "+y+ " té "+ x[y-1]+ " victòries");
            y = scanner.nextInt();
        }

        List<Integer> list = new ArrayList<Integer>();

        for (int i = 0; i < x.length; i++) {
            if(x[i] > x[i]+1)
                list.add(i);
        }

        System.out.println("L'equip guanyador és el "+ (list.size()+1));
    }
}
