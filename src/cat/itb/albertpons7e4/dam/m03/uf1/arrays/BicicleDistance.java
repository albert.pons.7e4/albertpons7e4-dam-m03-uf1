package cat.itb.albertpons7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class BicicleDistance {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double ms = scanner.nextDouble();
        double[] temps = {1,2,3,4,5,6,7,8,9,10};
        double tot;

        for (double temp : temps) {
            tot = ms * temp;
            System.out.print(tot);
            System.out.print(" ");
        }
        System.out.println();
    }
}
