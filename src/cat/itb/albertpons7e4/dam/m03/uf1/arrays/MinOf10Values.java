package cat.itb.albertpons7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class MinOf10Values {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] val = new int[10];
        for(int i = 0; i < val.length; i++){
            val[i] = scanner.nextInt();

            int min = val[0];
            for(i = 0; i<val.length; i++){
                if(val[i] < min)
                    min = val[i];
            }

            System.out.println(min);
        }
    }
}
