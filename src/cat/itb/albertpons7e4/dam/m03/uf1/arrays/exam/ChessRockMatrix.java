package cat.itb.albertpons7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ChessRockMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean[][] chess = new boolean[8][8];
        int p1 = scanner.nextInt();
        int p2 = scanner.nextInt();

        chess[p1+1][p2+1] = true;

        System.out.println(Arrays.toString(chess));
    }
}
