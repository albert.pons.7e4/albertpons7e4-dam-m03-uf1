package cat.itb.albertpons7e4.dam.m03.uf1.arrays;

public class MatrixElementSum {
    public static void main(String[] args) {

        int[][] matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}};
        int suma = 0;


        for (int[] ints : matrix) {
            for (int j = 0; j <= matrix.length; j++) {
                suma = suma + ints[j];
            }

        }
        System.out.println(suma);

    }
}
