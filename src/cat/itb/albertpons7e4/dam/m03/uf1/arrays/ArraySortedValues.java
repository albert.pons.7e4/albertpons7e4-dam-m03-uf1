package cat.itb.albertpons7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        boolean isOrdered = true;

        int[] valors = new int[n];
        for(int i = 1; i< valors.length; i++){
            if(valors[i]>valors[i+1])
                isOrdered = false;
        }

        if(isOrdered)
            System.out.println("ordenats");
        else
            System.out.println("desordenats");
    }
}
