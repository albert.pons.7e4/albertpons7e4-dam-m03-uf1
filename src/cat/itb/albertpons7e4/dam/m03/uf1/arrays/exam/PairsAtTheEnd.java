package cat.itb.albertpons7e4.dam.m03.uf1.arrays.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PairsAtTheEnd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> list = new ArrayList<>();
        int num = scanner.nextInt();

        while(num != -1){
            if (num % 2 == 0)
                list.add(0,num);
            else
                list.add(num);
            num = scanner.nextInt();
        }
        System.out.println(list);
    }
}
