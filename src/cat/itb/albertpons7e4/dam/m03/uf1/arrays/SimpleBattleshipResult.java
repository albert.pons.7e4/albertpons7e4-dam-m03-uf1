package cat.itb.albertpons7e4.dam.m03.uf1.arrays;


import java.util.Arrays;
import java.util.Scanner;

public class SimpleBattleshipResult {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int [][] tableru = new int[7][7];
        tableru[0][0] = 1;
        tableru[0][1] = 1;
        tableru[0][6] = 1;
        tableru[1][2] = 1;
        tableru[1][6] = 1;
        tableru[2][6] = 1;
        tableru[3][1] = 1;
        tableru[3][2] = 1;
        tableru[3][3] = 1;
        tableru[3][6] = 1;
        tableru[4][4] = 1;
        tableru[5][4] = 1;
        tableru[6][0] = 1;

        int fila = scanner.nextInt();
        int columna = scanner.nextInt();

        if(tableru[fila][columna] == 0)
            System.out.println("aigua");
        else
            System.out.println("tocat");
    }
}
