package cat.itb.albertpons7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class MatrixReader {
    /**
     * Reads an int matrix from user input.
     * The user first introduces the size of the matrix(NxM).
     * Then, introduces the integers one by one.
     * @return int[][] matrix of values introduced of size NxM
     */
    public static int[][] scannerReadIntMatrix(Scanner scanner) {
        int[][] matrix = new int[scanner.nextInt()][scanner.nextInt()];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++)
                matrix[i][j] = scanner.nextInt();
        }
        return matrix;
    }
}
