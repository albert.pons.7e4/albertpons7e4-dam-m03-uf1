package cat.itb.albertpons7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] array = new int[9];
        int num = scanner.nextInt();
        int num2 = scanner.nextInt();

        while (num !=-1 || num2 != -1){
            array[num] = num2;
            num = scanner.nextInt();
            num2 = scanner.nextInt();
        }

        System.out.println(Arrays.toString(array));
    }
}
