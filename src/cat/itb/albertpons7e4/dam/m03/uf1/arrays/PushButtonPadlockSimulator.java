package cat.itb.albertpons7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class PushButtonPadlockSimulator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int pushed;

        boolean[] num = new boolean[8];

        do{
            pushed = scanner.nextInt();
            num[pushed] = false;
        }while(pushed != -1);

        System.out.println(Arrays.toString(num));

    }



}
