package cat.itb.albertpons7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StoreFirstOnly {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();
        List<Integer> list = new ArrayList<Integer>();


        while (num != -1) {
            num = scanner.nextInt();

                if (!list.contains(num))
                    list.add(num);
            }
        System.out.println(list);
        }

    }
