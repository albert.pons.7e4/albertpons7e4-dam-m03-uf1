package cat.itb.albertpons7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num;
        int pos = 0;
        List<Integer> list = new ArrayList<Integer>();

        do{
            num = scanner.nextInt();
            if(num != -1)
                if(pos % 2 == 0)
                    list.add(0,num);
                else if(pos % 2 == 1)
                    list.add(num);
        pos++;

        }while(num != -1);

        System.out.println(list);
    }
}
