package cat.itb.albertpons7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AnotherInverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num;
        List<Integer> list = new ArrayList<>();
        num = scanner.nextInt();

        while(num != -1){
            list.add(num);
            num = scanner.nextInt();
        }

        for(int i = list.size() - 1; i >= 0; i--) {
            System.out.print(list.get(i));
            System.out.print(" ");
        }
        System.out.println();
    }

}
