package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class FoodFeeder {
    private static void printTotal(List<Animal> list) {
        int gTot = 0;
        for (Animal animal : list) {
            gTot += animal.total();
        }
        System.out.printf("Total: %dg", gTot);
    }

    private static void printMesGolos(List<Animal> list) {
        Animal a = list.stream().max(Comparator.comparing(Animal::total)).get();
        System.out.printf("Animal més golós: %s-%s", a.getCode(), a.getName());
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Animal> list = new ArrayList<>();

        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            String code = scanner.next();
            String name = scanner.next();
            int panxo = scanner.nextInt();
            int grams = scanner.nextInt();
            Animal a = new Animal(code, name, panxo, grams);
            list.add(a);
        }
        System.out.println("---------- Animals ----------\n");
        list.forEach(System.out::println);
        System.out.println("---------- Resum ----------");
        printTotal(list);
        System.out.println();
        printMesGolos(list);
    }
}
