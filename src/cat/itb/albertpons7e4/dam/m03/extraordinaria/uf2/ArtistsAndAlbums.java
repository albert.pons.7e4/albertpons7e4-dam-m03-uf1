package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArtistsAndAlbums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Artista> artistaList = new ArrayList<>();
        List<Album> albumList = new ArrayList<>();

        int maxL1 = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < maxL1; i++) {
            String nom = scanner.nextLine();
            String pais = scanner.nextLine();
            Artista a = new Artista(nom, pais);
            artistaList.add(a);
        }

        int maxL2 = scanner.nextInt();
        for (int i = 0; i < maxL2; i++) {
            int numArt = scanner.nextInt();
            scanner.nextLine();
            String nom = scanner.nextLine();
            int any = scanner.nextInt();
            Album a = new Album(artistaList.get(numArt), nom, any);
            albumList.add(a);
        }

        System.out.println("#### Albums");
        albumList.forEach(System.out::println);
    }
}
