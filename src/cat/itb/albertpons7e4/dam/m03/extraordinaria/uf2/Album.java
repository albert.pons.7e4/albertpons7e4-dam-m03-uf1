package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf2;

public class Album {
    Artista artista;
    String nom;
    int any;

    public Album(Artista artista, String nom, int any) {
        this.artista = artista;
        this.nom = nom;
        this.any = any;
    }

    public String getNom() {
        return nom;
    }

    @Override
    public String toString() {
        return String.format("%s (%s) - %s - %d", artista.nom, artista.pais, nom, any);
    }
}
