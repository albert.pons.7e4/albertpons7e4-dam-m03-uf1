package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf2;

public class Artista {
    String nom;
    String pais;

    public Artista(String nom, String pais) {
        this.nom = nom;
        this.pais = pais;
    }

    public String getNom() {
        return nom;
    }
}
