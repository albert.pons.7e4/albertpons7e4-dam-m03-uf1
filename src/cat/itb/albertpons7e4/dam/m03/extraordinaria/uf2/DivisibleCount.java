package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DivisibleCount {
    private static List<Integer> integerListMaker(int num) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < num + 1; i++) {
            list.add(i);
        }
        return list;
    }

    private static void printDivisibles(List<Integer> list,List<Integer> list2) {
        int cons = 0;
        for (Integer value : list) {
            for (Integer integer : list2) {
                if (value % integer == 0) {
                    cons++;
                }
            }
            System.out.printf("%d: %d\n", value, cons);
            cons = 0;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();
        int div = scanner.nextInt();
        int num = scanner.nextInt();

        while (!(num == -1)) {
            list.add(num);
            num = scanner.nextInt();
        }
        printDivisibles(list, integerListMaker(div));
    }
}
