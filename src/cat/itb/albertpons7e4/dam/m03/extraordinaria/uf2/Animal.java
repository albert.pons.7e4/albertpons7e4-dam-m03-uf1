package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf2;

public class Animal {
    String code;
    String name;
    int panxo;
    int grams;

    public Animal(String code, String name, int panxo, int grams) {
        this.code = code;
        this.name = name;
        this.panxo = panxo;
        this.grams = grams;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int total() {
        return (grams * panxo);
    }

    @Override
    public String toString() {
        return String.format("%s-%s %d àpats, %dg/apat, total %dg", code, name, panxo, grams, total());
    }
}
