package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf4.gestiousuaris;

public class Usuari {
    String nom;
    String llinatge1;
    String llinatge2;
    String dataNaixament;

    public Usuari(String nom, String llinatge1, String llinatge2, String dataNaixament) {
        this.nom = nom;
        this.llinatge1 = llinatge1;
        this.llinatge2 = llinatge2;
        this.dataNaixament = dataNaixament;
    }

    public String getNom() {
        return nom;
    }

    public String email() {
        return String.format("%s.%s@itb.cat", nom, llinatge1);
    }

    @Override
    public String toString() {
        return String.format("Nom: %s\nCognoms: %s %s\nData de naixament: %s\nCorreu electrònic: %s\n", nom, llinatge1, llinatge2, dataNaixament, email());
    }
}
