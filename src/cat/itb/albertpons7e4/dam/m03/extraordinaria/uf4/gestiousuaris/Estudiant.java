package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf4.gestiousuaris;

public class Estudiant extends Usuari{
    int anyMatriculacio;
    public Estudiant(String nom, String llinatge1, String llinatge2, String dataNaixament, int dataMatriculacio) {
        super(nom, llinatge1, llinatge2, dataNaixament);
        this.anyMatriculacio = dataMatriculacio;
    }

    @Override
    public String email() {
        return String.format("%s.%s.%s@itb.cat", nom, llinatge1, Integer.toHexString(anyMatriculacio));
    }
}
