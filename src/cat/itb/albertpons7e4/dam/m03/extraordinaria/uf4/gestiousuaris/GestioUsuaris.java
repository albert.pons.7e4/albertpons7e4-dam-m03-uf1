package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf4.gestiousuaris;

public class GestioUsuaris {
    public static void main(String[] args) {
        Usuari u1 = new Usuari("Dani", "Carrasco", "Morera", "04/09/1986");
        Usuari u2 = new Usuari("Laura", "Ivars", "Perelló", "17/05/1996");
        Estudiant e = new Estudiant("Aitana", "Sanchis", "Marí", "20/11/2001", 2020);

        System.out.println(u1);
        System.out.println(u2);
        System.out.println(e);
    }
}
