package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf4.filtretext;

public class ReverseFilter implements Filtre{
    String text;
    @Override
    public void setFiltre(String text) {
        this.text = text;
    }

    @Override
    public String filteredText(String text) {
        String revers = "";
        for (int i = text.length(); i == 0; i--) {
            revers = new StringBuilder().append(revers).append(text.charAt(i)).toString();
        }return revers;
    }

    @Override
    public void print() {
        System.out.println(text);
    }
}
