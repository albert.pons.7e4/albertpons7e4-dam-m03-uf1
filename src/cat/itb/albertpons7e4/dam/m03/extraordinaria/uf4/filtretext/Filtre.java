package cat.itb.albertpons7e4.dam.m03.extraordinaria.uf4.filtretext;

public interface Filtre {
     String text = null;
     void setFiltre(String text);
     String filteredText(String text);
     void print();
}
