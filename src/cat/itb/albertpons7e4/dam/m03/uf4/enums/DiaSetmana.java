package cat.itb.albertpons7e4.dam.m03.uf4.enums;

public enum DiaSetmana {
    DILLUNS,
    DIMARTS,
    DIMECRES,
    DIJOUS,
    DIVENDRES,
    DISSABTE,
    DIUMENGE
}
