package cat.itb.albertpons7e4.dam.m03.uf4.enums;

public class Classe {
    String horaInici;
    String hooraFinal;
    String materia;
    DiaSetmana dia;

    public Classe(String horaInici, String hooraFinal, String materia, DiaSetmana dia) {
        this.horaInici = horaInici;
        this.hooraFinal = hooraFinal;
        this.materia = materia;
        this.dia = dia;
    }

    @Override
    public String toString() {
        return String.format("%s %s - %s: %s", dia, horaInici, hooraFinal, materia);
    }

    public static void main(String[] args) {
        Classe m3 = new Classe("08:50", "10:30", "M3", DiaSetmana.DIMARTS);
        System.out.println(m3);
    }
}
