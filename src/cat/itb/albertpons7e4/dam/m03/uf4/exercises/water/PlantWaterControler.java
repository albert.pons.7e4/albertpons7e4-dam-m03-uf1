package cat.itb.albertpons7e4.dam.m03.uf4.exercises.water;

import java.util.List;

public class PlantWaterControler {
    public static double averageHumidity(List<Double> humidity){
        double sum = 0;
        for(double h : humidity){
            sum += h;
        }
        return sum / humidity.size();
    }
    public static void main(String[] args) {
        WaterSystem waterSystem = new MockedWaterSystem();
        List<Double> humidity = waterSystem.getHumidityRecord();
        System.out.println(humidity);
        double average = averageHumidity(humidity);
        System.out.println("Average: " + average);
        if(average < 2)
            waterSystem.startWaterSystem();
    }
}
