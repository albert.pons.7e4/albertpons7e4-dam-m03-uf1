package cat.itb.albertpons7e4.dam.m03.uf4.exercises.water;

import java.util.List;

public interface WaterSystem {
    List<Double> getHumidityRecord();
    void startWaterSystem();
}
