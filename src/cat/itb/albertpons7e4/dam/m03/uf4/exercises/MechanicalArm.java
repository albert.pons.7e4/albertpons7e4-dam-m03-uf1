package cat.itb.albertpons7e4.dam.m03.uf4.exercises;

public class MechanicalArm {
    protected double angle;
    protected double height;
    protected boolean turnedOn;

    public MechanicalArm(){
        this.angle = 0;
        this.height = 0;
        this.turnedOn = false;
    }

    public double getAngle() {
        return angle;
    }
    public double getHeight() {
        return height;
    }
    public boolean isTurnedOn() {
        return turnedOn;
    }

    private void setAngle(double angle) {
        if(isTurnedOn()) {
            this.angle = Math.max(angle, 0);
            this.angle = Math.min(this.angle, 360);
        }
    }
    private void setHeight(double height) {
        if(isTurnedOn()) {
            this.height = Math.max(height, 0);
        }
    }
    private void setTurnedOn(boolean turnedOn){
        this.turnedOn = turnedOn;
    }

    public void turnOn(){
        setTurnedOn(true);
    }
    public void turnOff(){
        setTurnedOn(false);
    }

    public void updateAngle(double angle){
        setAngle(getAngle() + angle);
    }
    public void updateHeight(double height){
        setHeight(getHeight() + height);
    }

    @Override
    public String toString() {
        return "MechanicalArm{" +
                "angle=" + angle +
                ", height=" + height +
                ", turnedOn=" + turnedOn +
                '}';
    }
}