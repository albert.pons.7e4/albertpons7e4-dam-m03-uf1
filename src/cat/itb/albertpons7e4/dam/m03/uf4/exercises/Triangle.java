package cat.itb.albertpons7e4.dam.m03.uf4.exercises;

public class Triangle extends Instrument{
    private int resonance;

    public Triangle(int resonance) {
        super();
        this.resonance = resonance;
    }

    @Override
    public String getSound(){
        String sound = "T";
        for (int i = 0; i < resonance; i++)
            sound += "I";
        sound += "NC";
        return sound;
    }
}
