package cat.itb.albertpons7e4.dam.m03.uf4.exercises.water;

import java.util.ArrayList;
import java.util.List;

public class MockedWaterSystem implements WaterSystem{
    List<Double> humidity;
    public MockedWaterSystem() {
        humidity = new ArrayList<>();
        generateRandomHumidity(20);
    }

    private void generateRandomHumidity(int n){
        for(int i = 0; i < n; i++){
            humidity.add(Math.random()*5);
        }
    }

    @Override
    public List<Double> getHumidityRecord(){
        return humidity;
    }

    public void startWaterSystem(){
        System.out.println("Water system started.");
    }
}
