package cat.itb.albertpons7e4.dam.m03.uf4.exercises;

public class Drum extends Instrument{
    private char tone;

    public Drum(char tone) {
        this.tone = tone;
    }

    @Override
    public String getSound(){
        String sound = "";
        for(int i = 0; i < 3; i++)
            sound += tone;
        sound = "T" + sound + "M";
        return sound;
    }
}
