package cat.itb.albertpons7e4.dam.m03.uf4.abstracts.figure;

import cat.itb.albertpons7e4.dam.m03.uf4.exercises.figures.ConsoleColors;

public abstract class Figure {
    private String color;

    protected Figure(String color){
        this.color = color;
    }

    private void aplicarColor(){
        System.out.print(this.color);
    }
    private void treureColor(){
        System.out.print(ConsoleColors.RESET);
    }

    protected abstract void draw();

    public void paint(){
        aplicarColor();
        draw();
        treureColor();
    }
}
