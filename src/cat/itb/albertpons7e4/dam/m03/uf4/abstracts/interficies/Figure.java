package cat.itb.albertpons7e4.dam.m03.uf4.abstracts.interficies;

public interface Figure {
    /**
     *
     * @return
     */
    public double area();
    public double perimeter();
}
