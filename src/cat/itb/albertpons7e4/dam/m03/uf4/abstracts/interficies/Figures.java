package cat.itb.albertpons7e4.dam.m03.uf4.abstracts.interficies;

import java.util.ArrayList;
import java.util.List;

public class Figures {
    public static void main(String[] args) {

        List<Figure> figures = new ArrayList<Figure>();
        figures.add(new Rectangle(10, 20));
        figures.add(new Cercle(1));

        for(Figure f : figures){
            System.out.println(f);
            System.out.printf("Area: %.2f\n", f.area());
            System.out.printf("Perimeter: %.2f\n", f.perimeter());
        }
    }
}
