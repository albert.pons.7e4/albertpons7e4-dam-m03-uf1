package cat.itb.albertpons7e4.dam.m03.uf6.generalexam;

import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data.Database;
import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.List;
import java.util.Scanner;

public class ListBooksByYearApp {
    private static BookDAO bookDAO;

    public ListBooksByYearApp() {
        this.bookDAO = new BookDAO();
    }

    private static void listByYear(int year) {
        List<Book> books = bookDAO.listByYear(year);
        System.out.println(books);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Database db = Database.getInstance();
        db.connect();

        System.out.println("Introdueix any:");
        int year = scanner.nextInt();
        System.out.println("## Books For Year " + year);
        listByYear(year);

        db.close();
    }
}
