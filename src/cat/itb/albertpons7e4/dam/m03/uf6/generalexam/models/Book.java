package cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models;

public class Book {
    String title;
    String author;
    String isbn;
    int year;
    int pages;

    public Book(String title, String author, String isbn, int year, int pages) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.year = year;
        this.pages = pages;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public int getPages() {
        return pages;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %d", getTitle(), getAuthor(), getYear());
    }
}
