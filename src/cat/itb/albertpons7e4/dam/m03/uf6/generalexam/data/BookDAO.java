package cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data;

import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDAO {
    public List<Book> listByYear(int any){
        List<Book> list = new ArrayList<>();
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM book WHERE year = " + any + " ORDER BY isbn";
            Statement listStatement = connection.createStatement();

            ResultSet resultat = listStatement.executeQuery(query);
            while (resultat.next()){
                String title = resultat.getString("title");
                String author = resultat.getString("author");
                String isbn = resultat.getString("isbn");
                int year = resultat.getInt("year");
                int pages = resultat.getInt("pages");

                list.add(new Book(title, author, isbn, year, pages));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    public void insert(Book b) {
        try {
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO book (title, author, isbn, year, pages) values (?, ?, ?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, b.getTitle());
            insertStatement.setString(2, b.getAuthor());
            insertStatement.setString(3, b.getIsbn());
            insertStatement.setInt(4, b.getYear());
            insertStatement.setInt(5, b.getPages());
            insertStatement.execute();
            System.out.println("## Book inserted");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void longest() {
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT DISTINCT(title), author, isbn, year, pages FROM book WHERE pages = (SELECT max(pages) FROM book) GROUP BY title, author, isbn, year";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            String title = resultat.getString("title");
            String author = resultat.getString("author");
            String isbn = resultat.getString("isbn");
            int year = resultat.getInt("year");
            int pages = resultat.getInt("pages");

            Book book = new Book(title,author, isbn, year, pages);

            System.out.println(book);
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
}
