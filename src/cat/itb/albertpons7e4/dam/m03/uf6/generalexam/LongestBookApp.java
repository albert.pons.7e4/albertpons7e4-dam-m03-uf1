package cat.itb.albertpons7e4.dam.m03.uf6.generalexam;

import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data.Database;
import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.List;
import java.util.Scanner;

public class LongestBookApp {
    private static BookDAO bookDAO;

    public LongestBookApp() {
        this.bookDAO = new BookDAO();
    }

    private static void listByYear(int year) {
        List<Book> books = bookDAO.listByYear(year);
        System.out.println(books);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Database db = Database.getInstance();
        db.connect();

        System.out.println("## Longest Book");
        longBook();

        db.close();
    }

    private static void longBook() {
        bookDAO.longest();
    }
}
