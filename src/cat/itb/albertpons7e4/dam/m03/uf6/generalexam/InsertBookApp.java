package cat.itb.albertpons7e4.dam.m03.uf6.generalexam;

import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.data.Database;
import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.Scanner;

public class InsertBookApp {
    private static BookDAO bookDAO;

    public InsertBookApp() {
        this.bookDAO = new BookDAO();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Database db = Database.getInstance();
        db.connect();

        System.out.println("Introdueix titol:");
        String title = scanner.next();
        System.out.println("Introdueix autor:");
        String author = scanner.next();
        System.out.println("Introdueix isbn:");
        String isbn = scanner.next();
        System.out.println("Introdueix any:");
        int year = scanner.nextInt();
        System.out.println("Introdueix pàgines:");
        int pages = scanner.nextInt();
        Book book = new Book(title, author, isbn, year, pages);
        insertBook(book);

        db.close();
    }

    private static void insertBook(Book b) {
        bookDAO.insert(b);
    }
}
