package cat.itb.albertpons7e4.dam.m03.uf2.staticfunctions;

import java.util.Locale;
import java.util.Scanner;

public class PersonValidator {
    /**
     * Returns true if is a valid phone number (composed only by digits, spaces or +)
     * @param phone
     * @return true if valid
     */
    public static boolean isValidPhoneNumber(String phone){
        for (int i = 0; i < phone.length(); i++) {
            char lletra = phone.charAt(i);
            if (!(Character.isDigit(lletra) || lletra == '+' || lletra == ' '))
                return false;
        }
        return true;
    }

    /**
     * Returns true if is a valid person name (composed only characters and starts with upper case)
     * @param personName
     * @return true if valid
     */
    public static boolean isValidPersonName(String personName){
        boolean isValid;
        isValid = personName.matches("[a-zA-Z]") && Character.isUpperCase(personName.charAt(0));
        return isValid;
    }

    /**
     * Returns true if is a valid dni (including correct letter)
     * @param dni
     * @return true if valid
     */
    public static boolean isValidDni(String dni){
        boolean isValid = false;
        return isValid;
    }

    /**
     * Returns true if is a valid postalCode
     * @param postalCode
     * @return true if valid
     */
    public static boolean isValidPostalcode(String postalCode){
        boolean isValid = false;
        if (postalCode.matches("\\d{5}") && Integer.parseInt(postalCode) >= 1000
        && postalCode.matches("\\d{5}") && Integer.parseInt(postalCode) <= 52999)
            isValid = true;
        return isValid;
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        String nom = lector.nextLine();
        String dni = lector.nextLine();
        String tlf = lector.nextLine();
        String postal = lector.nextLine();

        boolean esNomCorrecte = isValidPersonName(nom);
        boolean esValidDni = isValidDni(dni);
        boolean esValidTlf = isValidPhoneNumber(tlf);
        boolean esValidPostal = isValidPostalcode(postal);

        boolean esCorrecte = esNomCorrecte && esValidDni && esValidTlf && esValidPostal;
        if (esCorrecte)
            System.out.println("");
        else
            System.out.println("");
    }
}