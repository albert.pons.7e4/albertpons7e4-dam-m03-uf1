package cat.itb.albertpons7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntListReaderSample {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(lector);
        System.out.println(list);
    }
}
