package cat.itb.albertpons7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {
    public static List<Integer> readIntegerList(Scanner lector) {
        List<Integer> list = new ArrayList<Integer>();
        while(true) {
            int num = lector.nextInt();
            if (num == -1) {
                break;
            }
            list.add(num);
        }
        return list;
    }
    public static int min (List<Integer> list) {
        int min = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) < min)
                min = list.get(i);
        }
        return min;
    }
    public static int max (List<Integer> list) {
        int max = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) > max)
                max = list.get(i);
        }
        return max;
    }
    public static double avg (List<Integer> list) {
        double avg = 0;
        for (int i = 0; i < list.size(); i++) {
            avg += list.get(i);
        }
        avg /= list.size();
        return avg;
    }
}
