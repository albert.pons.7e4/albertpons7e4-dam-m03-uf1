package cat.itb.albertpons7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CovidApp {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> list = CovidCalculations.readDailyCasesFromScanner(lector);
        int totalCases = CovidCalculations.countTotalCases(list);
        double averageCases = CovidCalculations.average(list);
        List<Double> growthCases = CovidCalculations.growthRates(list);
        double lastGrowthRate = growthCases.get(growthCases.size()-1);

        System.out.printf("Hi ha hagut %d casos en total, amb una mitjana de %.2f per dia.\n" +
                "L'útlim creixement és de %.2f\n", totalCases, averageCases, lastGrowthRate);
    }
}
