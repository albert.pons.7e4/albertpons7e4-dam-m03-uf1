package cat.itb.albertpons7e4.dam.m03.uf2;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StudentStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

        int minim = IntegerLists.min(list);
        int maxima = IntegerLists.max(list);
        double average = IntegerLists.average(list);

        System.out.printf("Nota mínima: %d\n", minim);
        System.out.printf("Nota màxima: %d\n", maxima);
        System.out.printf("Nota mitjana: %.2f\n", average);
    }
}
