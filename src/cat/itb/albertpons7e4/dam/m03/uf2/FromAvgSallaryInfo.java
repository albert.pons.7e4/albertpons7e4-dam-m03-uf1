package cat.itb.albertpons7e4.dam.m03.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FromAvgSallaryInfo {

    public static void main(String[] args) {
        List<Salary> list = new ArrayList<Salary>();
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        /*Salary empleat = new Salary(scanner.nextDouble(), scanner.next());
        list.add(empleat);*/

        while(true){
            Salary empleat = new Salary(scanner.nextDouble(), scanner.nextLine());
            if (empleat.getSalari() == -1)
                break;
            else
                list.add(empleat);
        }

        /*for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }*/

        //System.out.println(Salary.mitjana(list));

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getSalari() < Salary.mitjana(list))
                System.out.println(list.get(i).getNom().trim());
        }
    }
}
