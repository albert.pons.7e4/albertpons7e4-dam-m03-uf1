package cat.itb.albertpons7e4.dam.m03.uf2;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CheapestPrice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        int minim = IntegerLists.min(list);
        System.out.printf("El producte més econòmic val: %d€\n", minim);
    }
}
