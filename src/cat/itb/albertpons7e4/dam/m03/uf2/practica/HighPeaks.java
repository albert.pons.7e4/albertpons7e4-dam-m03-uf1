package cat.itb.albertpons7e4.dam.m03.uf2.practica;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class HighPeaks {

    /**
     * Creates Cim with the info provided by the Scanner.
     * @param lector Scans keyboard input.
     * @return returns the new Cim created.
     */
    public static Cim cimReader(Scanner lector) {
        String nom = lector.nextLine();
        int metres = lector.nextInt();
        lector.nextLine();
        String ubicacio = lector.nextLine();
        int altura = lector.nextInt();
        int temps = lector.nextInt();
        lector.nextLine();
        return new Cim(nom, metres, ubicacio, altura, temps);
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        List<Cim> cims = new ArrayList<Cim>();
        int nCims = lector.nextInt();
        lector.nextLine();

        for (int i = 0; i < nCims; i++) {
            cims.add(cimReader(lector));
        }

        Cim.printCimsAconseguits(cims);
    }
}