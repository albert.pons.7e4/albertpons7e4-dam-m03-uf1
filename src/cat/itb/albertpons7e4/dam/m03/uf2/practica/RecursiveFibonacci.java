package cat.itb.albertpons7e4.dam.m03.uf2.practica;

import java.util.Locale;
import java.util.Scanner;

public class RecursiveFibonacci {
    /**
     * Asks for a number to check if exists in Fibonacci's sequence.
     * @param n The number to check if corresponds to a number of Fibonacci's sequence.
     * @return returns isFibonacci filling all extra parameters.
     */
    public static int isFibonacci(int n) {
        return isFibonacci(n, 0, 1);
    }
    /**
     * Does a recursive Fibonacci's sequence and it checks if the number given "n" is included in this sequence.
     *
     * @param n The number to check if it exists in Fibonacci's sequence.
     * @param i Corresponds to fibonacci - 2.
     * @param j Corresponds to fibonacci - 1.
     * @return Returns 1 if the number is on the sequence and 0 if it's not.
     */
    public static int isFibonacci(int n, int i, int j) {
        int fibonacci = i + j;
        i = j;
        j = fibonacci;
        if (fibonacci == n)
            return fibonacci;
        else if (n == 0 || n == 1)
            return 1;
        else if (i > n)
            return 0;

        return isFibonacci(n, i, j);
    }
    // Main
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        int n = lector.nextInt();
        System.out.println(isFibonacci(n) != 0);
    }
}