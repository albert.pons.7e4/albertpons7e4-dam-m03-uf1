package cat.itb.albertpons7e4.dam.m03.uf2.practica;

import java.util.Locale;
import java.util.Scanner;

public class RecursiveFactorial {
    /**
     * Does the factorial of a number.
     * @param n is the number given for the operation.
     * @return Returns (n * n-1 * n-2 * [...]) until it reaches 1. If n is 0, it will always returns 1.
     */
    public static int recursiveFact(int n) {
        if (n == 1)
            return n;
        else if (n == 0)
            return 1;
        return n * recursiveFact(n - 1);
    }
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        int i = lector.nextInt();
        // Loops 'i' number of times, 'i' being how many times you want to factorize values.
        while (i > 0) {
            int n = lector.nextInt();
            System.out.println(recursiveFact(n));
            i--;
        }
    }
}
