package cat.itb.albertpons7e4.dam.m03.uf2.practica;

import java.util.List;

public class Cim {
    private String nom;
    private int metres;
    private String ubicacio;
    private int altura;
    private int temps;

    /**
     * Constructor that creates an object called Cim referring to mountain peaks.
     * @param nom Name of the peak.
     * @param metres Meters of the route.
     * @param ubicacio Location where the peak is at.
     * @param altura Height of the peak.
     */
    public Cim(String nom, int metres, String ubicacio, int altura, int temps) {
        this.nom = nom;
        this.metres = metres;
        this.ubicacio = ubicacio;
        this.altura = altura;
        this.temps = temps;
    }
    // Getters
    public String getNom() {
        return nom;
    }
    public int getMetres() {
        return metres;
    }
    public String getUbicacio() {
        return ubicacio;
    }
    public int getAltura() {
        return altura;
    }
    public int getTemps() {
        return temps;
    }

    // Setters
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setMetres(int metres) {
        this.metres = metres;
    }
    public void setUbicacio(String ubicacio) {
        this.ubicacio = ubicacio;
    }
    public void setAltura(int altura) {
        this.altura = altura;
    }
    public void setTemps(int temps) {
        this.temps = temps;
    }

    /**
     * Sorts the highest peak on the list.
     * @param cims List of peaks.
     * @return Returns the object with the highest peak.
     */
    private static Cim highestCim(List<Cim> cims) {
        Cim max = cims.get(0);
        for (int i = 1; i < cims.size(); i++) {
            Cim j = cims.get(i);
            if (j.getAltura() > max.getAltura())
                max = j;
        }
        return max;
    }

    /**
     * Sorts the quickest peak done on the list.
     * @param cims List of peaks.
     * @return Returns the object with the quickest peak.
     */
    private static Cim quickestCim(List<Cim> cims) {
        Cim quickest = cims.get(0);
        for (int i = 1; i < cims.size(); i++) {
            Cim j = cims.get(i);
            if (j.getTemps() < quickest.getTemps())
                quickest = j;
        }
        return quickest;
    }

    /**
     * Sorts the fastest peak done on the list.
     * @param cims List of peaks.
     * @return Returns the object with the fastest peak.
     */
    private static Cim fastestCim(List<Cim> cims) {
        Cim fastest = cims.get(0);
        for (int i = 1; i < cims.size(); i++) {
            Cim j = cims.get(i);
            if (j.getMetres()/j.getTemps() < fastest.getMetres()/fastest.getTemps())
                fastest = j;
        }
        return fastest;
    }

    /**
     * It converts m/s to km/h
     * @return Returns the speed on km/h being 3.6 the conversion from m/s to km/h
     */
    private double getKmH() {
        return ((double) this.metres / (double) this.temps) * 3.6;
    }

    /**
     * Prints on screen all the information given by the user referring to mountain peaks.
     * @param cims List of peaks.
     */
    public static void printCimsAconseguits(List<Cim> cims) {
        System.out.println("------------------------");
        System.out.println("--- Cims aconseguits ---");
        System.out.println("------------------------");
        for (int i = 0; i < cims.size(); i++) {
            System.out.println(cims.get(i));
        }
        System.out.println("------------------------");
        System.out.printf("N. cims: %d\n", cims.size());
        System.out.printf("Cim més alt: %s (%dm)\n", highestCim(cims).getNom(), highestCim(cims).getMetres());
        System.out.printf("Cim més ràpid: %s (%d:%02d)\n", quickestCim(cims).getNom(), quickestCim(cims).getTemps()/60, quickestCim(cims).getTemps()%60);
        System.out.printf("Cim més veloç:  %s %.2fkm/hora\n", quickestCim(cims).getNom(), quickestCim(cims).getKmH());
        System.out.println("------------------------");
    }

    @Override
    public String toString() {
        return String.format("%s - %s (%dm) - Temps: %d:%02d", nom, ubicacio, metres, temps/60, temps%60);
    }
}
