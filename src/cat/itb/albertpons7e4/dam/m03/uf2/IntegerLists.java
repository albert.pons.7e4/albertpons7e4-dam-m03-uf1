package cat.itb.albertpons7e4.dam.m03.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<Integer>();

        while(true){
            int num = scanner.nextInt();
            if(num == -1)
                break;
            list.add(num);
        }

        return list;
    }

    /**
     * Donada una llista d'enters, ens retorna el valor mínim
     * @param list Llista d'enterns
     * @return Valor mínim
     */
    public static int min(List<Integer> list){
        int minim = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if(list.get(i) < minim){
                minim = list.get(i);
            }
        }
        return minim;
    }

    public static int max(List<Integer> list){
        int maxim = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if(list.get(i) > maxim){
                maxim = list.get(i);
            }
        }
        return maxim;
    }

    public static double average(List<Integer> list){
        int suma = 0;
        for(int n : list){
            suma += n;
        }
        double average = ((double) suma) / list.size();
        return average;
    }
}
