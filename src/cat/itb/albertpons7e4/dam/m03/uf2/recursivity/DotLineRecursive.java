package cat.itb.albertpons7e4.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class DotLineRecursive {

    public static String getDots(int n) {
        if (n == 1) {
            return ".";
        } else
            return "." + getDots(n-1);
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        int a = lector.nextInt();

        System.out.println(getDots(a));
    }
}
