package cat.itb.albertpons7e4.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class MultiplicationRecursive {

    public static int getMutiply(int a, int b) {
        if (b == 0)
            return 0;
        else
            return a + getMutiply(a, b-1);
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        int a = lector.nextInt();
        int b = lector.nextInt();

        System.out.println(getMutiply(a, b));
    }
}
