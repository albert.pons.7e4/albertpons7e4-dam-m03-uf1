package cat.itb.albertpons7e4.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class Npometes {
    public String getAppleSongStanza(int applesCount){
        return String.format("%1$d pometes té el pomer,%nde %1$d una, de %1$d una,%n" +
                "%1$d pometes té el pomer,%nde %1$d una en caigué.%nSi mireu el vent d'on vé%n" +
                "veureu el pomer com dansa,%nsi mireu el vent d'on vé%n" +
                "veureu com dansa el pomer.%n", applesCount);
    }

    public String getAppleSong(int applesCount) {
        if (applesCount >= 0)
            System.out.println(getAppleSongStanza(applesCount));
        else
            return "";
        return getAppleSong(applesCount-1);
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        Npometes song = new Npometes();
        int n = lector.nextInt();

        song.getAppleSong(n);
    }
}
