package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import java.util.Locale;
import java.util.Scanner;

public class BasicRobot {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        Robot robot = new Robot();
        String action = lector.next();
        while (!(action.equals("END"))) {
            switch (action) {
                case "DALT":
                    robot.moveUp();     // Moving on 'Y' axis
                    break;
                case "BAIX":
                    robot.moveDown();   // Moving on 'Y' axis
                    break;
                case "DRETA":
                    robot.moveRight();  // Move on 'X' axis
                    break;
                case "ESQUERRA":
                    robot.moveLeft();   // Move on 'X' axis
                    break;
                case "ACCELERAR":
                    robot.speedUp();    // Speed += 0.5
                    break;
                case "DISMINUIR":
                    robot.slowDown();  // Speed -= 0.5
                    break;
                case "POSICIO":
                    robot.printPosition();
                    break;
                case "VELOCITAT":
                    robot.printSpeed();
                    break;
            }
            action = lector.next();
        }
    }
}
