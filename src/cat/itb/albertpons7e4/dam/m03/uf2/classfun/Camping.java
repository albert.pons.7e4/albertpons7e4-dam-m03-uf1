package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;

public class Camping {
    List<Reserva> llistaReserves;

    public Camping() {
        this.llistaReserves = new ArrayList<Reserva>();
    }

    public void addReserva(Reserva r) {
        llistaReserves.add(r);
    }
    public void addReserva(String nom, int numeroPersones) {
        Reserva r = new Reserva(nom, numeroPersones);
        llistaReserves.add(r);
    }

    public void remove(String nom) {
        int i;
        boolean found = false;

        for (i = 0; i < llistaReserves.size(); i++) {
            Reserva r = llistaReserves.get(i);
            String nomReserva = r.getNomReserva();
            if (nomReserva.equals(nom)){
                found = true;
                break;
            }
        }

        if (found)
            llistaReserves.remove(i);
    }

    public int getNumeroParceles() {
        return llistaReserves.size();
    }

    public int getNumeroPersones() {
        int nPersones = 0;
        for(Reserva r : llistaReserves) {
            nPersones += r.getNumeroPersones();
        }
        return nPersones;
    }

    public void printInfo() {
        System.out.printf("parcel·les: %d\n", this.getNumeroParceles());
        System.out.printf("persones: %d\n", this.getNumeroPersones());
    }
}
