package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

public class Reserva {
    private String nomReserva;
    private int numeroPersones;

    public Reserva(String nomReserva, int numeroPersones) {
        this.nomReserva = nomReserva;
        this.numeroPersones = numeroPersones;
    }

    public String getNomReserva() {
        return nomReserva;
    }
    public int getNumeroPersones() {
        return numeroPersones;
    }

    public void setNomReserva(String nomReserva) {
        this.nomReserva = nomReserva;
    }
    public void setNumeroPersones(int numeroPersones) {
        this.numeroPersones = numeroPersones;
    }
}
