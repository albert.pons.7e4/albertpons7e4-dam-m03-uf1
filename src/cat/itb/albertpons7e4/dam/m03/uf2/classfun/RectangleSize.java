package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import cat.itb.albertpons7e4.dam.m03.uf2.classfun.Rectangle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RectangleSize {
    public static void main(String[] args) {
        List<Rectangle> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();

        for (int i = 0; i < num; i++) {
            Rectangle figura = new Rectangle(scanner.nextDouble(), scanner.nextDouble());
            list.add(figura);
        }

        for (Rectangle rectangle : list) {
            System.out.println("Un rectangle de " + rectangle.getBase() + " x " + rectangle.getAltura() + " té " + rectangle.getArea() + " d'area i " + rectangle.getPerimetre() + " de perímetre");
        }
    }
}
