package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;

public class Rectangle {
    private double base;
    private double altura;


    public Rectangle(double base, double altura){
        this.base = base;
        this.altura = altura;
    }

   public double getBase(){
        return base;
   }
   public double getAltura(){
        return altura;
    }

    public double getArea(){
        return base * altura;
    }
    public double getPerimetre(){
        return 2*(base+altura);
    }

}
