package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CampSiteOrganizer {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        Camping camping = new Camping();
        while (true) {
            String accio = lector.next();
            if (accio.equals("END"))
                break;

            if (accio.equals("ENTRA")) {
                int nPersones = lector.nextInt();
                String nom = lector.next();
                camping.addReserva(nom, nPersones);
            }else if (accio.equals("MARXA")){
                String nom = lector.next();
                camping.remove(nom);
            }

            camping.printInfo();
        }
    }
}
