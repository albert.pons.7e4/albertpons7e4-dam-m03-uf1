package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PeopleCounter {
    private int persona;

    public PeopleCounter(int persona) {
        this.persona = persona;
    }

    public int getPersona() {
        return persona;
    }

    public static PeopleCounter readPeople(Scanner scanner){
        int num = scanner.nextInt();
        return new PeopleCounter(num);
    }


    public static void main(String[] args) {
        List<PeopleCounter> list = new ArrayList<PeopleCounter>();
        Scanner scanner = new Scanner(System.in);

        /*int n = 2;
        while (n != -1) {
            // codi
            n = scanner.nextInt();
        }*/


        PeopleCounter a = readPeople(scanner);
        while (a.getPersona() != -1) {
            list.add(a);
            a = readPeople(scanner);
        }

        int total = 0;
        for (int i = 0; i < list.size(); i++) {
            total = total+list.get(i).getPersona();
        }

        System.out.println(total);
    }
}
