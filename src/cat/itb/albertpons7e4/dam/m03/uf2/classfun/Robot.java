package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

public class Robot {
    private double positionX;
    private double positionY;
    private double speed;

    public Robot() {
        this.positionX = 0;
        this.positionY = 0;
        this.speed = 1;
    }
    // Prints
    public void printPosition() {
        System.out.printf("La posició del robot és (%.1f, %.1f)\n", this.positionX, this.positionY);
    }
    public void printSpeed() {
        System.out.printf("La velocitat del robot és %.1f\n", this.speed);
    }
    // Setters
    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }
    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }
    public void setSpeed(double speed) {
        this.speed = speed;
    }
    // Actions
    public void moveUp() {
        setPositionY(this.positionY + this.speed);
    }
    public void moveDown() {
        setPositionY(this.positionY - this.speed);
    }
    public void moveRight() {
        setPositionX(this.positionX + this.speed);
    }
    public void moveLeft() {
        setPositionX(this.positionX - this.speed);
    }
    public void speedUp() {
        setSpeed(this.speed + 0.5);
        if (this.speed > 10)
            this.speed = 10;
    }
    public void slowDown() {
        setSpeed(this.speed - 0.5);
        if (this.speed < 0)
            this.speed = 0;
    }
}