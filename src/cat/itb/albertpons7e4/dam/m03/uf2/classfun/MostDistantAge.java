package cat.itb.albertpons7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class MostDistantAge {
    private int edat;

    public MostDistantAge(int edat) {
        this.edat = edat;
    }

    public static MostDistantAge ageReader(Scanner lector) {
        int edat = lector.nextInt();
        return new MostDistantAge(edat);
    }
    public static int getMin(List<MostDistantAge> list) {
        int min = list.get(0).getEdat();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEdat() < min)
                min = list.get(i).getEdat();
        }
        return min;
    }
    public static int getMax(List<MostDistantAge> list) {
        int max = list.get(0).getEdat();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEdat() > max)
                max = list.get(i).getEdat();
        }
        return max;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        List<MostDistantAge> list = new ArrayList<MostDistantAge>();
        MostDistantAge edat = ageReader(lector);

        while (edat.getEdat() != -1) {
            list.add(edat);
            edat = ageReader(lector);
        }

        int min = getMin(list);
        int max = getMax(list);
        int diferencia = max - min;

        System.out.printf("%d\n", diferencia);
    }
}
