package cat.itb.albertpons7e4.dam.m03.uf2;

public class Product {
    private String name;
    private double prize;

    public Product(String name, double prize){
        this.name = name;
        this.prize = prize;
    }

    public String getName(){
        return this.name;
    }

    public double getPrice(){
        return this.prize;
    }
    public void setName(String name){
        this.name = name;

    }

    public void setPrize(double prize){
        this.prize = prize;
    }
}
