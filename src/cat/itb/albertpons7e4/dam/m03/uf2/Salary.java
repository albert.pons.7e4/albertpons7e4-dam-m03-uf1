package cat.itb.albertpons7e4.dam.m03.uf2;

import java.util.List;

public class Salary {
    private double salari;
    private String nom;

    public Salary(double salari, String nom) {
        this.salari = salari;
        this.nom = nom;
    }

    public double getSalari() {
        return salari;
    }

    public String getNom() {
        return nom;
    }

    public static double mitjana(List<Salary> list) {
        double mitjana = 0;

        for (int i = 0; i < list.size(); i++) {
            mitjana += list.get(i).getSalari();
        }
        mitjana /= list.size();

        return mitjana;
    }

    @Override
    public String toString() {
        String str = String.format("Empleat: %s --> Salary: %.2f", nom, salari);
        return str;
    }
}
