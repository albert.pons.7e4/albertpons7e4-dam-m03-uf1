package cat.itb.albertpons7e4.dam.m03.uf2;

import java.util.Scanner;

public class OneProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String productName = scanner.next();
        double productPrize = scanner.nextDouble();
        Product producte = new Product(productName, productPrize);
        System.out.println("El producte val "+ producte.getPrice() +"€ i es diu: "+producte.getName());
    }
}

