package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class PeopleCounter {
    int nPersones;

    public PeopleCounter(int nPersones) {
        this.nPersones = nPersones;
    }

    public int getnPersones() {
        return nPersones;
    }

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        List<PeopleCounter> edifici = new ArrayList<PeopleCounter>();

        int nPersones = lector.nextInt();
        while (nPersones != -1) {
            PeopleCounter sala = new PeopleCounter(nPersones);
            edifici.add(sala);
            nPersones = lector.nextInt();
        }
        int total = 0;
        for (int i = 0; i < edifici.size(); i++) {
            total += edifici.get(i).getnPersones();
        }
        System.out.println(total);
    }
}
