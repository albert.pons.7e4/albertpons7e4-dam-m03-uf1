package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class Product{
    private String name;
    private double price;
    private int count;

    public static Product readProduct(Scanner scanner){
        int count = scanner.nextInt();
        String name = scanner.next();
        double prize = scanner.nextDouble();
        return new Product(name, prize, count);
    }
    /**
     * Product constructor. Creates a product given the name and prize of the product
     * @param name Product name
     * @param prize Product prize
     */
    public Product(String name, double prize){
        this(name, prize, 1);
        this.name = name;
        this.price = prize;
    }
    /**
     * Product constructor. Creates a product given the name and prize of the product
     * @param name Product name
     * @param price Product prize
     * @param count Product count (how many I have)
     */
    public Product(String name, double price, int count){
        this.name = name;
        this.price = price;
        this.count = count;
    }

    // Getters
    public String getName() {
        return name;
    }
    public double getPrice() {
        return price;
    }
    public int getCount() {
        return count;
    }
    // Setters
    public void setName(String name) {
        this.name = name;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString(){
        return String.format("%d %s (%.2f€)", count, name, price);
    }

    public double totalPrize(){
        return count * price;
    }

}