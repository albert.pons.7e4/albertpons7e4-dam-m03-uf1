package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class Rectangle {
    private double base;
    private double altura;

    public static Rectangle readRectangle(Scanner lector){
        double base = lector.nextDouble();
        double altura = lector.nextDouble();
        return new Rectangle(base, altura);
    }

    public Rectangle(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    public double getBase() {
        return base;
    }
    public double getAltura() {
        return altura;
    }
    public double getArea() {
        return base * altura;
    }
    public double getPerimetre() {
        return 2*(base + altura);
    }

    public void setBase(double base) {
        this.base = base;
    }
    public void setAltura(double altura) {
        this.altura = altura;
    }
}
