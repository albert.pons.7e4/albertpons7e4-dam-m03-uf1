package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class OneProductInfoPrinter {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        String name = lector.next();
        double price = lector.nextDouble();

        Product taula = new Product(name, price);
        System.out.printf("El producte %s val %.2f€\n", taula.getName(), taula.getPrice());
    }
}
