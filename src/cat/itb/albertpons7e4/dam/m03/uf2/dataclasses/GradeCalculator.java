package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class GradeCalculator {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        int numeroNotes = lector.nextInt();
        StudentGrade[] notes = new StudentGrade[numeroNotes];

        for (int i = 0; i < notes.length; i++) {
            notes[i] = StudentGrade.readStudent(lector);
        }

        for (int i = 0; i < notes.length; i++) {
            System.out.printf("%s: %.1f\n", notes[i].getNom(), notes[i].getNotaFinal());
        }
    }
}
