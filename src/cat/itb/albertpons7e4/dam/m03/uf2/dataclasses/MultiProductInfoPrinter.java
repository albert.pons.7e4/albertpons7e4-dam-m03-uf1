package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class MultiProductInfoPrinter {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        
        int numProducts = lector.nextInt();
        Product[] productes = new Product[numProducts];

        for (int i = 0; i < productes.length; i++) {
            String nom = lector.next();
            double preu = lector.nextDouble();
            productes[i] = new Product(nom, preu);
        }

        for (int i = 0; i < productes.length; i++) {
            System.out.printf("El producte %s val %.2f€\n", productes[i].getName(), productes[i].getPrice());
        }
    }
}
