package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class StudentInfoPrinter {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);

        int numEscoles = lector.nextInt();
        School[] escoles = new School[numEscoles];

        for (int i = 0; i < escoles.length; i++) {
            escoles[i] = School.readSchool(lector);
        }

        int numStudents = lector.nextInt();
        for (int i = 0; i < numStudents; i++) {
            int studentSchool = lector.nextInt();
            String student = lector.nextLine();

            escoles[studentSchool].setStudent(student);
        }

        for (int i = 0; i < escoles.length; i++) {
            School.printSchool(escoles[i]);
        }
    }
}
