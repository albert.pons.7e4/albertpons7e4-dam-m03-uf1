package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class School {
    private String name;
    private String address;
    private int postalCode;
    private String city;
    private String student = "";

    public School(String name, String address, int postalCode, String city) {
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.student = student;
    }

    public static School readSchool(Scanner lector) {
        String name = lector.nextLine();
        String address = lector.nextLine();
        int postalCode = lector.nextInt();
        String city = lector.nextLine();
        School school = new School(name, address, postalCode, city);
        return school;
    }
    public static void printSchool(School school) {
        System.out.println(school.toString());
    }

    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public int getPostalCode() {
        return postalCode;
    }
    public String getCity() {
        return city;
    }
    public String getStudent() {
        return student;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String adress) {
        this.address = adress;
    }
    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public String toString() {
        String str = String.format("%s\n" +
                "adreça: %s\n" +
                "codipostal: %d\n" +
                "ciutat: %s\n" +
                "%s", name, address, postalCode, city, student);
        return str;
    }
}
