package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class RectangleSize {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        int numRectangle = lector.nextInt();
        Rectangle[] rectangleList = new Rectangle[numRectangle];

        for (int i = 0; i < rectangleList.length; i++) {
            rectangleList[i] = Rectangle.readRectangle(lector);
        }

        for (int i = 0; i < rectangleList.length; i++) {
            System.out.printf("Un rectangle de %.1f x %.1f té %.1f d'area i %.1f de perímetre.\n",
                    rectangleList[i].getBase(), rectangleList[i].getAltura(),
                    rectangleList[i].getArea(), rectangleList[i].getPerimetre());
        }
    }
}
