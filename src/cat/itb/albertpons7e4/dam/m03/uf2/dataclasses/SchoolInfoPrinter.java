package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class SchoolInfoPrinter {
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        School itb = School.readSchool(lector);
        School.printSchool(itb);
    }
}
