package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FromAvgSallaryInfo {
    public static double getListAvg(List<Sallary> list) {
        double avg = 0;
        for (int i = 0; i < list.size(); i++) {
            avg += list.get(i).getSou();
        }
        avg /= list.size();
        return avg;
    }
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        List<Sallary> empSal = new ArrayList<Sallary>();

        // Stores employees names and salaries into a dynamic array list.
        while (true) {
            empSal.add(Sallary.readEmployee(lector));
            if (empSal.get(empSal.size()-1).getSou() == -1)
                break;
        }
        empSal.remove(empSal.get(empSal.size()-1));

        // Prints salaries under the average salary from all the employees.
        for (int i = 0; i < empSal.size(); i++) {
            if (empSal.get(i).getSou() < FromAvgSallaryInfo.getListAvg(empSal))
                System.out.println(empSal.get(i).getNom().trim());
        }
    }
}