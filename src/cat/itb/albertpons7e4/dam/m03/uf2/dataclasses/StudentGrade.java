package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class StudentGrade {
    private String nom;
    private double notaExercicis;
    private double notaExamen;
    private double notaProjecte;

    public static StudentGrade readStudent(Scanner lector){
        String nom = lector.next();
        double notaExercicis = lector.nextDouble();
        double notaExamen = lector.nextDouble();
        double notaProjecte = lector.nextDouble();
        StudentGrade nota =  new StudentGrade(nom, notaExercicis, notaExamen, notaProjecte);
        return nota;
    }

    public StudentGrade(String nom, double notaExercicis, double notaExamen, double notaProjecte) {
        this.nom = nom;
        this.notaExercicis = notaExercicis;
        this.notaExamen = notaExamen;
        this.notaProjecte = notaProjecte;
    }

    public String getNom() {
        return nom;
    }
    public double getNotaExercicis() {
        return notaExercicis;
    }
    public double getNotaExamen() {
        return notaExamen;
    }
    public double getNotaProjecte() {
        return notaProjecte;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setNotaExercicis(double notaExercicis) {
        this.notaExercicis = notaExercicis;
    }
    public void setNotaExamen(double notaExamen) {
        this.notaExamen = notaExamen;
    }
    public void setNotaProjecte(double notaProjecte) {
        this.notaProjecte = notaProjecte;
    }

    /**
     * Mètode que ens calcula la nota final de l'estudiant.
     * @return Nota final amb decimals.
     */
    public double getNotaFinal(){
        double notaFinal = 0.3 * this.notaExercicis + 0.3 * this.notaExamen + 0.4 * this.notaProjecte;
        return notaFinal;
    }

    public String toString(){
        String str = String.format("%s (%.1f %.1f %.1f)", nom, notaExercicis, notaExamen, notaProjecte);
        return str;
    }
}
