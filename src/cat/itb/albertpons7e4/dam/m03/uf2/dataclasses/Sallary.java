package cat.itb.albertpons7e4.dam.m03.uf2.dataclasses;

import java.util.List;
import java.util.Scanner;

public class Sallary {
    private int sou;
    private String nom;

    public static Sallary readEmployee(Scanner lector) {
        int sou = lector.nextInt();
        String nom = lector.nextLine();
        Sallary employee = new Sallary(sou, nom);
        return employee;
    }

    public Sallary(int sou, String nom) {
        this.sou = sou;
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
    public int getSou() {
        return sou;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        String str = String.format("Empleat: %s -> Sou: %d€", nom, sou);
        return str;
    }
}
