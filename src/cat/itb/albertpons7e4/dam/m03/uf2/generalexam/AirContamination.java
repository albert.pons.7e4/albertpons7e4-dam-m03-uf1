package cat.itb.albertpons7e4.dam.m03.uf2.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AirContamination {
    private static AirContamination airContamination;
    public List<Air> air;
    Scanner scanner;
    Air aire;

    public AirContamination() {
        air = new ArrayList<Air>();
        aire = new Air();
    }
    public void addAir(int cont){
        Air a = new Air(cont);
        air.add(a);
    }
    public void showAir(){
        for (int i = 0; i < air.size(); i++) {
            System.out.println(air.get(i));
        }
    }

    public int mitjana(){
        int mitj = 0;
        for (int i = 0; i < air.size(); i++) {
            mitj += air.get(i).contamination;
        }
        return mitj;
    }

    public int diesPlus(){
        int dies = 0;
        for (int i = 0; i < air.size(); i++) {
            //if (air.get(i)> airContamination.mitjana())  no va :(
                dies += 1;
        }
        return dies;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int max = scanner.nextInt();
        for (int i = 0; i < max; i++) {
            airContamination.addAir(scanner.nextInt());
        }
        System.out.println("Contaminació mitjana: " + AirContamination.airContamination.mitjana());
        System.out.println("Dies amb amb contaminació superior: " + airContamination.diesPlus());
    }
}
