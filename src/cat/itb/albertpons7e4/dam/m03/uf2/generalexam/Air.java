package cat.itb.albertpons7e4.dam.m03.uf2.generalexam;

public class Air {
    int contamination;

    public Air(int contamination){
        this.contamination = contamination;
    }

    public Air() {

    }

    public int getContamination() {
        return contamination;
    }

    public void setContamination(int contamination) {
        this.contamination = contamination;
    }

    @Override
    public String toString() {
        return contamination + "";
    }
}
