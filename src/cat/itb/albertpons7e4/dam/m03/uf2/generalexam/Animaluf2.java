package cat.itb.albertpons7e4.dam.m03.uf2.generalexam;

public class Animaluf2 {
    int id;
    String nomC;
    String nom;
    int avistaments;

    public Animaluf2(int id, String nomC, String nom) {
        this.id = id;
        this.nomC = nomC;
        this.nom = nom;
        this.avistaments = avistaments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomC() {
        return nomC;
    }

    public void setNomC(String nomC) {
        this.nomC = nomC;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAvistaments() {
        return avistaments;
    }

    public void setAvistaments(int avistaments) {
        this.avistaments = avistaments;
    }

    @Override
    public String toString() {
        return String.format("%s %s %d %d\n",nomC,nom,id,avistaments);
    }
}
