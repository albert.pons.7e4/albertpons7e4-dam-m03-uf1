package cat.itb.albertpons7e4.dam.m03.uf2.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EndangeredAnimalSights {
    private static EndangeredAnimalSights endangeredAnimalSights;
    Animaluf2 animal;
    public static List<Animaluf2> animals;
    Scanner scanner;

    public EndangeredAnimalSights(){
        animals = new ArrayList<Animaluf2>();
    }

    public void addAnimal(String nameC, String name){
        int id = animals.size();
        Animaluf2 a = new Animaluf2(id, nameC, name);
        animals.add(a);
    }
    public void avistaments(int id,int num){
        if (animal.getId() == id)
            animal.setAvistaments(num);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int max1 = 0;
        for (int i = 0; i < max1; i++) {
            EndangeredAnimalSights.endangeredAnimalSights.addAnimal(scanner.nextLine(), scanner.nextLine());
        }
        int max2 = 0;
        for (int i = 0; i < max2; i++) {
            endangeredAnimalSights.avistaments(scanner.nextInt(), scanner.nextInt());
        }

        System.out.println("--- Avistaments ---");
        for (int i = 0; i < animals.size(); i++) {
            System.out.println(animals.get(i).getNomC() + " - " + animals.get(i).getNom() + ": " + animals.get(i).getAvistaments());
        }
    }
}
