package cat.itb.albertpons7e4.dam.m03.uf2.generalexam;

public class Liberty {
    String pais;
    int casos;
    double gravetat;
    double puntuacio;

    public Liberty(String pais,int casos, double gravetat) {
        this.pais = pais;
        this.casos = casos;
        this.gravetat = gravetat;
        this.puntuacio = puntuacio;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public int getCasos() {
        return casos;
    }

    public void setCasos(int casos) {
        this.casos = casos;
    }

    public double getGravetat() {
        return gravetat;
    }

    public void setGravetat(int gravetat) {
        this.gravetat = gravetat;
    }

    public double getPuntuacio() {
        return puntuacio;
    }

    public void setPuntuacio(int puntuacio) {
        this.puntuacio = puntuacio;
    }

    @Override
    public String toString() {
        return String.format("%s %d %d %d\n",pais,casos,gravetat,puntuacio);
    }
}
