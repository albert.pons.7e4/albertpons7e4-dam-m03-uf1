package cat.itb.albertpons7e4.dam.m03.uf5.regex;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Replace {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Pattern emailPattern = Pattern.compile("(\\w+(?:\\.\\w+)*)@(\\w+(?:\\.\\w+)+)");

        String text = scanner.nextLine();
        Matcher matcher = emailPattern.matcher(text);
        while(matcher.find()){
            String email = matcher.group();
            String user = matcher.group(1);
            String domini = matcher.group(2);
            System.out.println("Email found: " + email);
            System.out.println("Usuari: " + user);
            System.out.println("Domini: " + domini);
        }
    }
}

