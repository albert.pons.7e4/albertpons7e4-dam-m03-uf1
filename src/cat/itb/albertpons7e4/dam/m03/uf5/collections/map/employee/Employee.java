package cat.itb.albertpons7e4.dam.m03.uf5.collections.map.employee;

import java.util.Objects;
import java.util.Scanner;

public class Employee {
    private String id;
    private String name;
    private String surname;
    private String address;

    public Employee(String id, String name, String surname, String address) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.address = address;
    }

    public static Employee readEmployee(Scanner scanner){
        String id = scanner.nextLine();
        String name = scanner.nextLine();
        String surname = scanner.nextLine();
        String address = scanner.nextLine();
        return new Employee(id, name, surname, address);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return String.format("%s %s - %s, %s", name, surname, id, address);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, address);
    }
}

