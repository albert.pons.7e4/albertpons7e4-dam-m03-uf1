package cat.itb.albertpons7e4.dam.m03.uf5.collections.queue;

public class Customer {
    private String name;
    private int productCount;

    public Customer(String name, int productCount) {
        this.name = name;
        this.productCount = productCount;
    }

    public String getName() {
        return name;
    }
    public int getProductCount() {
        return productCount;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", productCount=" + productCount +
                '}';
    }
}
