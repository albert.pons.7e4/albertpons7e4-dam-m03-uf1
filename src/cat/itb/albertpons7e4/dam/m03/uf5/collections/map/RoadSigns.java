package cat.itb.albertpons7e4.dam.m03.uf5.collections.map;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class RoadSigns {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<Integer, String> signs = new HashMap<>();

        int signCount = scanner.nextInt();
        for(int i = 0; i < signCount; i++){
            int km = scanner.nextInt();
            String name = scanner.next();
            signs.put(km, name);
        }

        while (true){
            int km = scanner.nextInt();
            if(km == -1)
                break;

            String name = signs.get(km);
            System.out.println(name == null ? "No hi ha cartell" : name);
        }
    }
}
