package cat.itb.albertpons7e4.dam.m03.uf5.generalexam;

import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SearchByISBN {
    public static void addBook(List<Book> list, String name, String author, String isbn, int pages, int year) {
        Book book = new Book(name, author, isbn, pages, year);
        list.add(book);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Book> books = new ArrayList<>();

        int numBooks = scanner.nextInt();

        for (int i = 0; i < numBooks; i++) {
            scanner.nextLine();
            String title = scanner.nextLine();
            String author = scanner.nextLine();
            String isbn = scanner.nextLine();
            int pages = scanner.nextInt();
            int year = scanner.nextInt();

            addBook(books, title, author, isbn, pages, year);
        }
        int numIsbn = scanner.nextInt();

        for (int i = 0; i < numIsbn; i++) {
            String code = scanner.nextLine();
            books.stream().filter(x -> x.getIsbn().equals(code)).forEach(System.out::println);
        }
    }
}
