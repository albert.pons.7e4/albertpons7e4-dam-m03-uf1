package cat.itb.albertpons7e4.dam.m03.uf5.generalexam;

import cat.itb.albertpons7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.*;

import static cat.itb.albertpons7e4.dam.m03.uf5.generalexam.SearchByISBN.addBook;

public class BookRanking {
    private static Book maxPages(List<Book> list) {
        return list.stream().max(Comparator.comparing(Book::getPages)).get();
    }

    private static void ordered(List<Book> list) {
        list.stream().sorted(Comparator.comparingInt(Book::getYear).thenComparing(Book::getTitle).reversed()).forEach(System.out::println);
    }

    private static double avgPages(List<Book> list) {
        return list.stream().mapToInt(Book::getPages).sum();
    }

    private static void afterYear(List<Book> list) {
        for (Book book : list) {
            if (book.getYear() > 2018)
                System.out.println(book);
        }
    }

    private static void moreThanHundred(List<Book> list) {
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getPages() > 100)
                System.out.println(list.get(i).getTitle());
        }
    }

    public static int pagPerYear(List<Book> list) {
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            int num = list.get(i).getPages() * list.get(i).getYear();
            sum = sum + num;
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Book> books = new ArrayList<>();
        int numBooks = scanner.nextInt();

        for (int i = 0; i < numBooks; i++) {
            scanner.nextLine();
            String title = scanner.nextLine();
            String author = scanner.nextLine();
            String isbn = scanner.nextLine();
            int pages = scanner.nextInt();
            int year = scanner.nextInt();

            addBook(books, title, author, isbn, pages, year);
        }
        maxPages(books);
        System.out.println("### Llibre amb més pàgines");
        System.out.println(maxPages(books));
        System.out.println();

        System.out.println("### Llibres per any");
        ordered(books);
        System.out.println();

        System.out.println("### Mitjana de pàgines");
        System.out.printf("%.1f",avgPages(books) / books.size());
        System.out.println();
        System.out.println();

        System.out.println("### Llibres publicats després del 2018");
        afterYear(books);
        System.out.println();

        System.out.println("###Suma de anys per pàgines");
        System.out.println(pagPerYear(books));
        System.out.println();

        System.out.println("###Més de 100 pàgines");
        moreThanHundred(books);
    }
}
