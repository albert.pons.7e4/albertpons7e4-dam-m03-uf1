package cat.itb.albertpons7e4.dam.m03.uf5.practica.ui;

public class Printer {
    /**
     * prints a string between lines
     */
    public static void printBetweenLines(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        for (int i = 0; i < lineSize; i++) {
            line.append("-");
        }
        System.out.println(line+"\n"+"    "+string+"\n"+line);
    }
}
