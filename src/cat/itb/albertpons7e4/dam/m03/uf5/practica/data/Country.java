package cat.itb.albertpons7e4.dam.m03.uf5.practica.data;

//this class is the country that I use all over this project
public class Country {
    String name;
    String code1;
    String code2;
    int newConfirmedCases;
    int totalConfirmedCases;
    int newDeaths;
    int totalDeaths;
    int newRecovered;
    int totalRecovered;
    int population;

//constructor
    public Country(String name, String code1, int newConfirmedCases, int totalConfirmedCases, int newDeaths, int totalDeaths, int newRecovered, int totalRecovered) {
        this.name = name;
        this.code1 = code1;
        this.newConfirmedCases = newConfirmedCases;
        this.totalConfirmedCases = totalConfirmedCases;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
    }

//getters

    public String getName() {
        return name;
    }
    public String getCode1() {
        return code1;
    }
    public int getNewConfirmedCases() {
        return newConfirmedCases;
    }
    public int getTotalConfirmedCases() {
        return totalConfirmedCases;
    }
    public int getNewDeaths() {
        return newDeaths;
    }
    public int getTotalDeaths() {
        return totalDeaths;
    }
    public int getNewRecovered() {
        return newRecovered;
    }
    public int getTotalRecovered() {
        return totalRecovered;
    }
    public int getPopulation() {
        return population;
    }
    public String getCode2() {
        return code2;
    }

    //setters
    public void setPopulation(int population) {
        this.population = population;
    }
    public void setCode2(String code2) {
        this.code2 = code2;
    }

    //toString
    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", code1='" + code1 + '\'' +
                ", code2='" + code2 + '\'' +
                ", newConfirmedCases=" + newConfirmedCases +
                ", totalConfirmedCases=" + totalConfirmedCases +
                ", newDeaths=" + newDeaths +
                ", totalDeaths=" + totalDeaths +
                ", newRecovered=" + newRecovered +
                ", totalRecovered=" + totalRecovered +
                ", population=" + population +
                '}';
    }
}
