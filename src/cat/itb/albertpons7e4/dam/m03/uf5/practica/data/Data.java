package cat.itb.albertpons7e4.dam.m03.uf5.practica.data;

import java.util.*;
import java.util.stream.Collectors;

public class Data {
    private static Data data;

    //get instance below is a method that makes the attributes created on this class to work on every other class you use it
    public static Data getInstance() {
        if (data == null)
            data = new Data();
        return data;
    }

    //lists and hashes
    public List<Country>
            countryList;
    public HashMap<String, String>
            populationHash;
    public HashMap<String, String>
            countryCodeHash;
    public List<String>
            euCountriesList;
    public List<Country>
            euFullCountries;

    //data
    public Data() {
        countryList = new ArrayList<>();
        countryCodeHash = new HashMap<>();
        populationHash = new HashMap<>();
        euCountriesList = new ArrayList<>();
        euFullCountries = new ArrayList<>();
    }

    //adders are the methods to add data into the lists or hashmaps that I use
    public void addDataCountries(String name, String code, int newConfirmedCases, int totalConfirmedCases, int newDeaths, int totalDeaths, int newRecovered, int totalRecovered) {
        Country a = new Country(name, code, newConfirmedCases, totalConfirmedCases, newDeaths, totalDeaths, newRecovered, totalRecovered);
        countryList.add(a);
    }

    public void addEuCountries(String code) {
        euCountriesList.add(code);
    }

    public void fillEuListV2() {
        euFullCountries = countryList.stream().filter(x -> euCountriesList.contains(x.code1)).collect(Collectors.toList());
    }

    public void addCodeOfThree() {
        euFullCountries.
                forEach(x -> {
                    countryCodeHash.entrySet().stream().filter(key -> key.getKey().equals(x.getCode1()))
                            .forEach(row -> x.setCode2(row.getValue()));
                });
    }

    public void addPopulation() {
        euFullCountries.
                forEach(x -> {
                    populationHash.entrySet().stream().filter(key -> key.getKey().equals(x.getCode2())).
                            forEach(row -> x.setPopulation(Integer.parseInt(row.getValue())));
                });
    }

    //totals
    //new
    public int totalNewConfirmed() {
        return countryList.stream().mapToInt(Country::getNewConfirmedCases).sum();
    }

    public int totalNewdeath() {
        return countryList.stream().mapToInt(Country::getNewDeaths).sum();
    }

    public int totalNewrecovered() {
        return countryList.stream().mapToInt(Country::getNewRecovered).sum();
    }

    //total
    public int totalConfirmed() {
        return countryList.stream().mapToInt(Country::getTotalConfirmedCases).sum();
    }

    public int totalDeath() {
        return countryList.stream().mapToInt(Country::getTotalDeaths).sum();
    }

    public int totalRecovered() {
        return countryList.stream().mapToInt(Country::getTotalRecovered).sum();
    }

    //tops
    public Optional<Country> topNewCases() {
        return countryList.stream().max(Comparator.comparing(Country::getNewConfirmedCases));
    }

    public Optional<Country> topNewDeath() {
        return countryList.stream().max(Comparator.comparing(Country::getNewDeaths));
    }

    public Optional<Country> topNewRecovered() {
        return countryList.stream().max(Comparator.comparing(Country::getNewRecovered));
    }

    public Optional<Country> topConfirmed() {
        return countryList.stream().max(Comparator.comparing(Country::getTotalConfirmedCases));
    }

    public Optional<Country> topDeaths() {
        return countryList.stream().max(Comparator.comparing(Country::getTotalDeaths));
    }

    public Optional<Country> topRecovered() {
        return countryList.stream().max(Comparator.comparing(Country::getTotalRecovered));
    }

    //tops EU
    public Optional<Country> topNewEuCases() {
        return euFullCountries.stream().max(Comparator.comparing(Country::getNewConfirmedCases));
    }

    public Optional<Country> topNewEuDeath() {
        return euFullCountries.stream().max(Comparator.comparing(Country::getNewDeaths));
    }

    public Optional<Country> topNewEuRecovered() {
        return euFullCountries.stream().max(Comparator.comparing(Country::getNewRecovered));
    }

    public Optional<Country> topEuConfirmed() {
        return euFullCountries.stream().max(Comparator.comparing(Country::getTotalConfirmedCases));
    }

    public Optional<Country> topEuDeaths() {
        return euFullCountries.stream().max(Comparator.comparing(Country::getTotalDeaths));
    }

    public Optional<Country> topEuRecovered() {
        return euFullCountries.stream().max(Comparator.comparing(Country::getTotalRecovered));

    }

    //find key is a class that I made for searching a key in a hashmap given any type of variable, in this case I did not use it
    public static <K, T> Optional<K> findKey(Map<K, T> mapOrNull, T value) {
        return Optional.ofNullable(mapOrNull).flatMap(map -> map.entrySet().stream()
                .filter(e -> Objects.equals(e.getValue(), value)).map(Map.Entry::getKey).findAny());
    }

    //Spain by population
    public double[] spain() {
        double[] percent = new double[6];
        int ncc = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getNewConfirmedCases();
        int tcc = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getTotalConfirmedCases();
        int nd = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getNewDeaths();
        int td = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getTotalDeaths();
        int nr = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getNewRecovered();
        int tr = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getTotalRecovered();
        int p = Objects.requireNonNull(euFullCountries.stream().filter(x -> x.code2.equals("ESP")).findFirst().orElse(null)).getPopulation();

        percent[0] = (double)(ncc * 100) / p;
        percent[1] = (double)(tcc * 100) / p;
        percent[2] = (double)(nd * 100) / p;
        percent[3] = (double)(td * 100) / p;
        percent[4] = (double)(nr * 100) / p;
        percent[5] = (double)(tr * 100) / p;

        return percent;
    }

    //Europe by population
    public Country[] onlyEu() {
        Country[] countries = new Country[6];
        Country nc = Objects.requireNonNull(euFullCountries.stream().max(Comparator.comparingDouble(x -> ((double) x.getNewConfirmedCases() * 100) / x.getPopulation())).orElse(null));
        Country tc = Objects.requireNonNull(euFullCountries.stream().max(Comparator.comparingDouble(x -> ((double) x.getTotalConfirmedCases() * 100) / x.getPopulation())).orElse(null));
        Country nd = Objects.requireNonNull(euFullCountries.stream().max(Comparator.comparingDouble(x -> ((double) x.getNewDeaths() * 100) / x.getPopulation())).orElse(null));
        Country td = Objects.requireNonNull(euFullCountries.stream().max(Comparator.comparingDouble(x -> ((double) x.getTotalDeaths() * 100) / x.getPopulation())).orElse(null));
        Country nr = Objects.requireNonNull(euFullCountries.stream().max(Comparator.comparingDouble(x -> ((double) x.getNewRecovered() * 100) / x.getPopulation())).orElse(null));
        Country tr = Objects.requireNonNull(euFullCountries.stream().max(Comparator.comparingDouble(x -> ((double) x.getTotalRecovered() * 100) / x.getPopulation())).orElse(null));

        countries[0] = nc;
        countries[1] = tc;
        countries[2] = nd;
        countries[3] = td;
        countries[4] = nr;
        countries[5] = tr;

        return countries;
    }
}
