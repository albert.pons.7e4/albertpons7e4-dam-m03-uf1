package cat.itb.albertpons7e4.dam.m03.uf5.practica.io;

import cat.itb.albertpons7e4.dam.m03.uf5.practica.data.Data;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

//In this class i create the lists and maps used for this project
public class Io {
    //to list
    public static void fromCoviddata() {
        Path path = Path.of("files/coviddata.txt");
        try {
            Scanner scannerFile = new Scanner(path);
            while (scannerFile.hasNext()) {
                String name = scannerFile.nextLine();
                String code = scannerFile.nextLine();
                int newConfirmedCases = scannerFile.nextInt();
                int totalConfirmedCases = scannerFile.nextInt();
                int newDeaths = scannerFile.nextInt();
                int totalDeaths = scannerFile.nextInt();
                int newRecovered = scannerFile.nextInt();
                int totalRecovered = scannerFile.nextInt();
                if (scannerFile.hasNextLine()) {
                    scannerFile.nextLine();
                }
                Data.getInstance().addDataCountries(name, code, newConfirmedCases, totalConfirmedCases, newDeaths, totalDeaths, newRecovered, totalRecovered);
            }
        } catch (IOException e) {
            System.err.println("WRONG ROUTE OR DATA");
        }
    }

    public static void fromEu() {
        Path path = Path.of("files/eu_countries.txt");
        try {
            Scanner scannerFile = new Scanner(path);
            while (scannerFile.hasNext()) {
                String code = scannerFile.nextLine();
                Data.getInstance().addEuCountries(code);
            }
        } catch (IOException e) {
            System.err.println("WRONG ROUTE OR DATA");
        }
    }
    //to hashmap
    public static void fromPopulationV2() throws IOException {
        String delimiter = " ";

        try(Stream<String> lines = Files.lines(Paths.get("files/population.txt"))){
            lines.filter(line -> line.contains(delimiter)).forEach(
                    line -> Data.getInstance().populationHash.putIfAbsent(line.split(delimiter)[0], line.split(delimiter)[1])
            );
        }
    }

    public static void fromCountryCodes() throws IOException {
        String delimiter = " ";

        try(Stream<String> lines = Files.lines(Paths.get("files/country_codes.txt"))){
            lines.filter(line -> line.contains(delimiter)).forEach(
                    line -> Data.getInstance().countryCodeHash.putIfAbsent(line.split(delimiter)[0], line.split(delimiter)[1])
            );
        }
    }
}