package cat.itb.albertpons7e4.dam.m03.uf5.practica.ui;

import java.util.Locale;
import java.util.Scanner;

//This class is a Scanner like class that i can create new methods on and use the ones that the previous class has
public class MyScanner {
    Scanner scanner;

    public MyScanner() {
        this.scanner = new Scanner(System.in).useLocale(Locale.US);
    }

    public int maxNum(int numMax){
        int accio = scanner.nextInt();
        while(accio < 0 || accio >numMax){
            System.out.println("ERROR");
            accio = scanner.nextInt();
        }
        return accio;
    }
    public String next(){
        return scanner.next();
    }
    public int nextInt(){
        return scanner.nextInt();
    }


    public Scanner getDefaultScanner() {
        return scanner;
    }
}
