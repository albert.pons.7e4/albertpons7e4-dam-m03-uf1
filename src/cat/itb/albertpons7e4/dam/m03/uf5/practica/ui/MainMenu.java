package cat.itb.albertpons7e4.dam.m03.uf5.practica.ui;

import cat.itb.albertpons7e4.dam.m03.uf5.practica.data.Data;

import static cat.itb.albertpons7e4.dam.m03.uf5.practica.ui.Printer.printBetweenLines;

public class MainMenu {
    MyScanner reader;

    //this method is the main menu that I use
    public MainMenu(MyScanner reader){
        this.reader = reader;
    }
    public void showMenu(){
        while (true) {
            printBetweenLines("MAIN MENU");
            System.out.println("CHOOSE AN OPTION");
            System.out.println("1.TOTAL DATA");
            System.out.println("2.TOPS");
            System.out.println("3.EU TOPS");
            System.out.println("4.SPAIN BY POPULATION");
            System.out.println("5.TOP EUROPEAN COUNTRIES BY POPULATION");
            System.out.println("0.EXIT");

            int acction = reader.maxNum(5);
            switch (acction) {
                case 1:
                    printBetweenLines("TOTAL DATA");
                    System.out.printf("Total of new cases: %s\n", Data.getInstance().totalNewConfirmed());
                    System.out.printf("Total of new deaths: %s\n", Data.getInstance().totalNewdeath());
                    System.out.printf("Total of new recovered: %s\n", Data.getInstance().totalNewrecovered());
                    System.out.printf("Total confirmed: %s\n", Data.getInstance().totalConfirmed());
                    System.out.printf("Total deaths: %s\n", Data.getInstance().totalDeath());
                    System.out.printf("Total recovered: %s\n", Data.getInstance().totalRecovered());
                    System.out.println();
                    break;
                case 2:
                    printBetweenLines("TOPS");
                    System.out.printf("The country with most new cases is %s with: %d \n",
                            Data.getInstance().topNewCases().get().getName(), Data.getInstance().topNewCases().get().getNewConfirmedCases());
                    System.out.printf("The country with most new Deaths is %s with: %d \n",
                            Data.getInstance().topNewDeath().get().getName(), Data.getInstance().topNewDeath().get().getNewDeaths());
                    System.out.printf("The country with most new cases is %s with: %d \n",
                            Data.getInstance().topNewRecovered().get().getName(), Data.getInstance().topNewRecovered().get().getNewRecovered());
                    System.out.printf("The country with most total cases is %s with: %d \n",
                            Data.getInstance().topConfirmed().get().getName(), Data.getInstance().topConfirmed().get().getTotalConfirmedCases());
                    System.out.printf("The country with most total deaths is %s with: %d \n",
                            Data.getInstance().topDeaths().get().getName(), Data.getInstance().topDeaths().get().getTotalDeaths());
                    System.out.printf("The country with most total recovered is %s with: %d \n",
                            Data.getInstance().topRecovered().get().getName(), Data.getInstance().topRecovered().get().getTotalRecovered());
                    System.out.println();
                    break;
                case 3:
                    printBetweenLines("EU TOPS");
                    System.out.printf("The european country with most new cases is %s with: %d \n",
                            Data.getInstance().topNewEuCases().get().getName(), Data.getInstance().topNewEuCases().get().getNewConfirmedCases());
                    System.out.printf("The european country with most new Deaths is %s with: %d \n",
                            Data.getInstance().topNewEuDeath().get().getName(), Data.getInstance().topNewEuDeath().get().getNewDeaths());
                    System.out.printf("The european country with most new cases is %s with: %d \n",
                            Data.getInstance().topNewEuRecovered().get().getName(), Data.getInstance().topNewEuRecovered().get().getNewRecovered());
                    System.out.printf("The european country with most total cases is %s with: %d \n",
                            Data.getInstance().topEuConfirmed().get().getName(), Data.getInstance().topEuConfirmed().get().getTotalConfirmedCases());
                    System.out.printf("The european country with most total deaths is %s with: %d \n",
                            Data.getInstance().topEuDeaths().get().getName(), Data.getInstance().topEuDeaths().get().getTotalDeaths());
                    System.out.printf("The european country with most total recovered is %s with: %d \n",
                            Data.getInstance().topEuRecovered().get().getName(), Data.getInstance().topEuRecovered().get().getTotalRecovered());
                    System.out.println();
                    break;
                case 4:
                    printBetweenLines("SPAIN BY POPULATION");

                    System.out.printf("New relative cases: %.3f%%\n", Data.getInstance().spain()[0]);
                    System.out.printf("Total relative cases: %.3f%%\n", Data.getInstance().spain()[1]);
                    System.out.printf("New relative deaths: %.3f%%\n", Data.getInstance().spain()[2]);
                    System.out.printf("Total relative deaths: %.3f%%\n", Data.getInstance().spain()[3]);
                    System.out.printf("New relative Recovered: %.3f%%\n", Data.getInstance().spain()[4]);
                    System.out.printf("Total relative Recovered: %.3f%%\n", Data.getInstance().spain()[5]);
                    break;
                case 5:
                    printBetweenLines("PERCENTS EUROPE");
                    System.out.printf("New relative cases: country %s with %.3f%%\n",
                            Data.getInstance().onlyEu()[0].getName(), ((double) Data.getInstance().onlyEu()[0].getNewConfirmedCases() * 100 / Data.getInstance().onlyEu()[0].getPopulation()));
                    System.out.printf("New relative cases: country %s with %.3f%%\n",
                            Data.getInstance().onlyEu()[1].getName(), ((double) Data.getInstance().onlyEu()[1].getTotalConfirmedCases() * 100 / Data.getInstance().onlyEu()[1].getPopulation()));
                    System.out.printf("New relative cases: country %s with %.3f%%\n",
                            Data.getInstance().onlyEu()[2].getName(), ((double) Data.getInstance().onlyEu()[2].getNewDeaths() * 100 / Data.getInstance().onlyEu()[2].getPopulation()));
                    System.out.printf("New relative cases: country %s with %.3f%%\n",
                            Data.getInstance().onlyEu()[3].getName(), ((double) Data.getInstance().onlyEu()[3].getTotalDeaths() * 100 / Data.getInstance().onlyEu()[3].getPopulation()));
                    System.out.printf("New relative cases: country %s with %.3f%%\n",
                            Data.getInstance().onlyEu()[4].getName(), ((double) Data.getInstance().onlyEu()[4].getNewRecovered() * 100 / Data.getInstance().onlyEu()[4].getPopulation()));
                    System.out.printf("New relative cases: country %s with %.3f%%\n",
                            Data.getInstance().onlyEu()[5].getName(), ((double) Data.getInstance().onlyEu()[5].getTotalRecovered() * 100 / Data.getInstance().onlyEu()[5].getPopulation()));
                    break;
                case 0:
                    return;
            }
        }
    }
}
