package cat.itb.albertpons7e4.dam.m03.uf5.practica;

import cat.itb.albertpons7e4.dam.m03.uf5.practica.data.Data;
import cat.itb.albertpons7e4.dam.m03.uf5.practica.io.Io;
import cat.itb.albertpons7e4.dam.m03.uf5.practica.ui.MainMenu;
import cat.itb.albertpons7e4.dam.m03.uf5.practica.ui.MyScanner;
import java.io.IOException;

//here I call all the classes and methods
public class CovidStatsApp {
    public static void main(String[] args) throws IOException {
        MyScanner myScanner = new MyScanner();
        MainMenu mainMenu = new MainMenu(myScanner);
        Io.fromCoviddata();
        Io.fromPopulationV2();
        Io.fromCountryCodes();
        Io.fromEu();
        Data.getInstance().fillEuListV2();
        Data.getInstance().addCodeOfThree();
        Data.getInstance().addPopulation();
        mainMenu.showMenu();
    }
}
