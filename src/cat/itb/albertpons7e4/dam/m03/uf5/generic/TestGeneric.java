package cat.itb.albertpons7e4.dam.m03.uf5.generic;


import java.util.Arrays;
import java.util.List;

public class TestGeneric {
    public static <T> T getFirst(List<T> list){
        return list.get(0);
    }
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(8, 4, 7, 9, 3, 4, 5);
        Integer firstInteger = getFirst(integers);
        System.out.println(firstInteger);

        List<String> strings = Arrays.asList("Joan", "Pere", "Maria");
        String firstString = getFirst(strings);
        System.out.println(firstInteger);
    }
}
