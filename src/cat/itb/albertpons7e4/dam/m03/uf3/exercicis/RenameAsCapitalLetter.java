package cat.itb.albertpons7e4.dam.m03.uf3.exercicis;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RenameAsCapitalLetter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String folderPath = scanner.nextLine();
        Path folder = Path.of(folderPath);

        try {
            Stream<Path> filesStream = Files.list(folder);
            List<Path> files = filesStream.collect(Collectors.toList());
            System.out.println(files);

            for (Path original : files) {
                Path filename = original.getFileName();
                String newFilename = filename.toString().substring(0,1).toUpperCase()+filename.toString().substring(1);
                Path newFilenamePath = folder.resolve(newFilename);
                Files.move(original, newFilenamePath);
                System.out.println(newFilenamePath);
            }
        } catch (IOException e) {
            System.err.println("maaaaal");
        }
    }
}
