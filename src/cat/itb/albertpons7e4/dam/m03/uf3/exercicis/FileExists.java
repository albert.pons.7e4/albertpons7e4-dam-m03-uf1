package cat.itb.albertpons7e4.dam.m03.uf3.exercicis;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class FileExists {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String pathE = scanner.nextLine();
        Path path = Path.of(pathE);
        boolean pathYes = Files.exists(path);
        System.out.printf("%s: %s\n",pathE ,pathYes);
    }
}
