package cat.itb.albertpons7e4.dam.m03.uf3.exercicis;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

    public class ProfileBackup {
        public static void main(String[] args) {
            String homePath = System.getProperty("user.home");
            Path home = Path.of(homePath, "m3");

            String now = LocalDateTime.now().toString();
            Path backupDirectory = Path.of(home.toString(), "backup", now);
            // Creem carpeta backup
            try {
                if (!Files.exists(backupDirectory)) {
                    Files.createDirectories(backupDirectory);
                    System.out.printf("%s creat correctament.\n", backupDirectory);
                } else {
                    System.err.printf("%s ja existeix.\n", backupDirectory);
                }
            }catch (IOException e){
                System.err.println("Error al crear la carpeta "+ backupDirectory);
            }

            Path profileOrigin = home.resolve(".profile");
            Path profileDestination = backupDirectory.resolve(".profile");
            try {
                Files.copy(profileOrigin, profileDestination);
                System.out.printf("S'ha copiat el fitxer %s a %s correctament\n", profileOrigin, profileDestination);
            }catch (IOException e){
                System.err.printf("Error al copiar el fitxer %s a %s\n", profileOrigin, profileDestination);
            }

        }
}
