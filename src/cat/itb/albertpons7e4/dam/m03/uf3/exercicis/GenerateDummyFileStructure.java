package cat.itb.albertpons7e4.dam.m03.uf3.exercicis;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class GenerateDummyFileStructure {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String homePath = System.getProperty("user.home");
        Path home = Path.of(homePath);

        Path dummyDirectory = home.resolve("dummyfolders");
        try {
            Files.createDirectory(dummyDirectory);
            System.out.printf("%s creat correctament.\n", dummyDirectory);
        } catch (IOException e){
            System.err.println("Error al crear la carpeta ~/dummyfolders");
        }

        for( int i = 1; i <= 100; i++) {
            try {
                Path subfolder = dummyDirectory.resolve(Integer.toString(i));
                if(!Files.exists(subfolder)) {
                    Files.createDirectory(subfolder);
                    System.out.printf("%s creat correctament.\n", subfolder);
                } else {
                    System.err.printf("%s ja existeix.\n", subfolder);
                }
            } catch (IOException e) {
                System.err.println("Error al crear la carpeta ~/dummyfolders/" + i);
            }
        }

    }
}
