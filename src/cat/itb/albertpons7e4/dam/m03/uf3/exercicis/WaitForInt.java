package cat.itb.albertpons7e4.dam.m03.uf3.exercicis;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WaitForInt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try{
            int num = scanner.nextInt();
            System.out.println("Número: "+num);
        }catch (InputMismatchException inputMismatchException){
            System.out.println("No és un enter");
        }
    }
}
