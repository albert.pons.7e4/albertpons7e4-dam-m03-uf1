package cat.itb.albertpons7e4.dam.m03.uf3.generalexam;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class CopyToUserFolder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String homePath = System.getProperty("user.home");
        Path file = Path.of(homePath).resolve(".bashrc");

        String ruta = scanner.nextLine();
        String nom = scanner.nextLine();
        String cognom = scanner.nextLine();

        Path first = Path.of(ruta);
        Path second = first.resolve(cognom);
        Path last = second.resolve(nom);

        try{
            Files.createDirectory(second);
            Files.createDirectory(last);
            Files.copy(file,last);
            System.out.println("Fitxer copiat a la carpeta" + last.toString());
        }catch (IOException e){
            System.err.println("Has introduit un path erroni");
            System.err.println(file.toString());
            System.err.println(last.toString());
        }
    }
}
