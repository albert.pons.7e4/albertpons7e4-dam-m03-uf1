package cat.itb.albertpons7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileCounterLog {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String homePath = System.getProperty("user.home");
        Path home = Path.of(homePath);
        Path fitxer = home.resolve("counterlog.txt");

        String ruta = scanner.nextLine();
        Path path = Path.of(ruta);

        try {
            Stream<Path> filesStream = Files.list(path);
            List<Path> files = filesStream.collect(Collectors.toList());

            OutputStream outputStream = Files.newOutputStream(fitxer, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            PrintStream printStream = new PrintStream(outputStream, true);

            String data = LocalDateTime.now().toString();
            printStream.println(data + " - " + "Tens " + files.size() + " Fitxers a " + path.toString());
        }catch (IOException e){
            System.err.println("Error obrint el fitxer: " + fitxer);
        }
    }
}

