package cat.itb.albertpons7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentGradesFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        Path path = Path.of(input);

        List<String> noms = new ArrayList<>();
        List<Double> notes = new ArrayList<>();

        try {
            Scanner scannerFitxer = new Scanner(path);

            int num = scannerFitxer.nextInt();

            while (scannerFitxer.hasNext()){

                String n = scannerFitxer.next();
                noms.add(n);

                double nota1 = scannerFitxer.nextDouble();
                double nota2 = scannerFitxer.nextDouble();
                double nota3 = scannerFitxer.nextDouble();
                double notaFinal = nota(nota1, nota2, nota3);

                notes.add(notaFinal);
            }
            int aprovats = 0;
            int suspesos = 0;

            for (int i = 0; i < notes.size(); i++) {


                if (notes.get(i) > 5)
                    suspesos++;
                else
                    aprovats++;
            }

            for (int i = 0; i < num; i++) {
                System.out.println("----- Estudiants -----");
                System.out.println(noms.get(i)+": "+notes.get(i));
            }
            System.out.println("----- Resum -----");
            System.out.println("Aprovats: " + aprovats);
            System.out.println("Suspesos: "+suspesos);

        } catch (IOException e){
            System.err.println("Has introduit un fitxer o dades erronees");
        }
    }

    public static double nota(double exs, double exam, double proj){
        exs = exs*0.3;
        exam = exam*0.3;
        proj = proj*0.4;

        return exs+exam+proj;
    }
}
