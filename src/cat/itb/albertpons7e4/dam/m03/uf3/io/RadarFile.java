package cat.itb.albertpons7e4.dam.m03.uf3.io;

import cat.itb.albertpons7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class RadarFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //Path fitxer = Path.of(scanner.next());
        Path fitxer = Path.of("/home/joapuiib/m3/radar.txt");

        List<Integer> velocitats = new ArrayList<Integer>();
        try {
            Scanner scannerFitxer = new Scanner(fitxer);

            while (scannerFitxer.hasNext()){
                int n = scannerFitxer.nextInt();
                velocitats.add(n);
            }

        } catch (IOException e){
            System.out.println(e);
        }

        int max = IntegerLists.max(velocitats);
        int min = IntegerLists.min(velocitats);
        //double average = IntegerLists.average(velocitats);
        System.out.printf("Velocitat màxima: %d\n", max);
        System.out.printf("Velocitat mínima: %d\n", min);
        //System.out.printf("Velocitat mitjana: %.2f\n", average);
    }
}

