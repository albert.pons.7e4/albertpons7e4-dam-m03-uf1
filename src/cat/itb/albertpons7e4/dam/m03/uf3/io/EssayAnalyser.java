package cat.itb.albertpons7e4.dam.m03.uf3.io;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

public class EssayAnalyser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Path fitxer = Path.of(scanner.next());

        try {
            Scanner scannerFitxer = new Scanner(fitxer);
            int nombreLinies = 0;
            int nombreParaules = 0;

            while(scannerFitxer.hasNextLine()){
                String line = scannerFitxer.nextLine();
                nombreLinies++;

                String[] paraules = line.split(" ");
                nombreParaules += paraules.length;
            }

            System.out.println("Número de línies: " + nombreLinies);
            System.out.println("Número de paraules: " + nombreParaules);

        }catch (IOException e){

        }
    }
}
