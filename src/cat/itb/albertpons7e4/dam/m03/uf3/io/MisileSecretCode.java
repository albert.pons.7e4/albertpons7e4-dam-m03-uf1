package cat.itb.albertpons7e4.dam.m03.uf3.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class MisileSecretCode {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Path home = Path.of("C:\\Users\\albert\\IdeaProjects\\albertpons7e4-dam-m03-uf1\\src\\cat\\itb\\albertpons7e4\\dam\\m03\\uf3\\io");
        Path file = home.resolve("secret.txt");

        try {
            if (Files.exists(home)){
                if(!Files.exists(file))
                    Files.createFile(file);

                OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                PrintStream printStream = new PrintStream(outputStream, true);

                printStream.println(scanner.next());
                System.out.println("missil preparat");
            }
        }catch (IOException e){
            System.err.println("mec");
        }
    }
}
